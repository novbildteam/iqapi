import django_filters
from django.db.models import Q
from rest_framework.filters import BaseFilterBackend
from reports.models import Lessons, Streams

class IsOwnerLessonsFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        return queryset.filter(room__owner=request.user)


class IsOwnerStreamsFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        return queryset.filter(lesson__room__owner=request.user)


class MultiValuesFilter(django_filters.Filter):

    def filter(self, qs, value):
        if not value:
            return qs

        values = value.split(',')
        union = None
        for v in values:
            arg = {self.name: v}
            if not union:
                union = Q(**arg)
            else:
                union = union | Q(**arg)
        qs = qs.filter(union)
        print(union)
        return qs

class LessonsFilter(django_filters.rest_framework.FilterSet):

    lesson_id = MultiValuesFilter(name='lesson_id')
    title = MultiValuesFilter(name='title')
    # begin_time = django_filters.NumberFilter(name='begin_time')
    # users = django_filters.NumberFilter(label='users')
    # title = django_filters.CharFilter(name='title')
    # streams_count = django_filters.NumberFilter(name='streams_count')
    # user_id = django_filters.NumberFilter()
    class Meta:
        model = Lessons
        fields = ('lesson_id', )


class StreamsFilter(django_filters.rest_framework.FilterSet):
    stream_id = MultiValuesFilter()

    class Meta:
        model = Streams
        fields = ('stream_id', )