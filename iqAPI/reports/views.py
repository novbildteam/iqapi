import django_filters
from django.http import HttpResponse
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets, status
from rest_framework.filters import OrderingFilter
from rest_framework.decorators import list_route, detail_route
from reports.models import Lessons, Sessions, Streams
from reports.serializers import \
    LessonsSerializer, SessionUserSerializer, LessonStreamsSerializer,\
    StreamQuestionsResultsSerializer, TestContentSerializer, StreamUsersResultsSerializer
from reports.filters import IsOwnerLessonsFilter, IsOwnerStreamsFilter, LessonsFilter, StreamsFilter

from os import fstat

from . import xlsx

@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def test_view(request):
    return Response({
        'message': 'hello world!',
    })


class LessonsViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Lessons.objects.prefetch_related('streams_set__sessions_set')

    permission_classes = (IsAuthenticated,)
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend,
                       OrderingFilter,
                       IsOwnerLessonsFilter,)

    serializer_class = LessonsSerializer
    #filter_fields = ('lesson_id',)
    filter_class = LessonsFilter
    ordering_fields = ('lesson_id', 'stream_id')


    @detail_route(methods=['get'])
    def users(self, request, *args, **kwargs):
        lesson = self.get_object()

        # distinct with "field" works only in PostgreSQL
        queryset = Sessions.objects.filter(stream__lesson=lesson).distinct('working_user_id')

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = SessionUserSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = SessionUserSerializer(queryset, many=True)

        return Response({'count': queryset.count(), 'results': serializer.data})

    @detail_route(methods=['get'], ordering_fields=('stream_id',))
    def streams(self, request, *args, **kwargs):
        lesson = self.get_object()

        queryset = Streams.objects.filter(lesson=lesson)
        queryset = OrderingFilter().filter_queryset(request, queryset.all(), self)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = LessonStreamsSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = LessonStreamsSerializer(queryset, many=True)
        return Response({'count': queryset.count(), 'results': serializer.data})


class StreamsViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Streams.objects

    permission_classes = (IsAuthenticated,)

    filter_backends = (django_filters.rest_framework.DjangoFilterBackend,
                       OrderingFilter,
                       IsOwnerStreamsFilter)

    serializer_class = LessonStreamsSerializer

    filter_class = StreamsFilter

    ordering_fields = ('stream_id', 'lesson_id', )

    @detail_route(methods=['get'])
    def users_results(self, *args, **kwargs):
        stream = self.get_object()
        results_data = StreamUsersResultsSerializer(stream).data
        return Response(results_data)


    @detail_route(methods=['get'])
    def questions_results(self, *args, **kwargs):
        stream = self.get_object()
        results_data = StreamQuestionsResultsSerializer(stream).data
        return Response(results_data)

    @detail_route(methods=['get'])
    def full_results(self, *args, **kwargs):
        stream = self.get_object()
        # TODO: need StreamFullResultsSerializer
        return Response({
            'users': StreamUsersResultsSerializer(stream).data,
            'questions': StreamQuestionsResultsSerializer(stream).data
        })

    @detail_route(methods=['get'])
    def test(self, *args, **kwargs):
        stream = self.get_object()
        test_content = TestContentSerializer(stream.test).data
        return Response(test_content)


    @detail_route(methods=['get'])
    def download(self, request, *args, **kwargs):
        stream = self.get_object()
        accepted_download_formats = ('xlsx',)
        format = request.GET.get('type')
        if format not in accepted_download_formats:
            return Response(status=status.HTTP_404_NOT_FOUND)
        # this data collection is redundant. should be optimized
        stream_data = LessonStreamsSerializer(stream).data
        users_data = StreamUsersResultsSerializer(stream).data
        questions_data = StreamQuestionsResultsSerializer(stream).data
        if format == 'xlsx':
            data = xlsx.StreamReport(stream_data, users_data, questions_data).data
            filesize = fstat(data.fileno()).st_size
            response = HttpResponse(data,
                                    content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
            response['Content-Length'] = filesize
            response['Content-Disposition'] = 'attachment; filename={}.xlsx'.format(stream_data['stream_id'])
            return response
