from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
import reports.views as views


router = DefaultRouter()
router.register(r'lessons', views.LessonsViewSet)
router.register(r'streams', views.StreamsViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
]