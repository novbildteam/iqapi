from django.db import models
from iqAuth.models import Users

# Create your models here.
class Rooms(models.Model):
    room_id = models.AutoField(primary_key=True)
    owner = models.ForeignKey(Users, models.DO_NOTHING)
    room_pass = models.CharField(max_length=100, blank=True, null=True)
    room_access = models.SmallIntegerField()
    room_status = models.BooleanField()
    queue = models.TextField(blank=True, null=True)  # This field type is a guess.
    show_results = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 'rooms'


class Branches(models.Model):
    branch_id = models.AutoField(primary_key=True)
    owner_id = models.IntegerField()
    branch_type = models.SmallIntegerField()
    current = models.IntegerField(blank=True, null=True)
    branch_name = models.CharField(max_length=150, blank=True, null=True)
    creation_time = models.DateTimeField(blank=True, null=True)
    editing_time = models.DateTimeField(blank=True, null=True)
    deleted = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'branches'


class TestsC(models.Model):
    test_c_id = models.AutoField(primary_key=True)
    test_description = models.TextField(blank=True, null=True)
    test_random = models.NullBooleanField()
    test_timer = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tests_c'


class Tests(models.Model):
    test_id = models.AutoField(primary_key=True)
    test_c = models.ForeignKey('TestsC', models.DO_NOTHING, blank=True, null=True)
    branch = models.ForeignKey(Branches, models.DO_NOTHING, blank=True, null=True)
    positions = models.TextField(blank=True, null=True)  # This field type is a guess.
    creation_time = models.DateTimeField(blank=True, null=True)
    deleted = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tests'


class Override(models.Model):
    override_id = models.AutoField(primary_key=True)
    room_id = models.IntegerField()
    test_id = models.IntegerField()
    position = models.SmallIntegerField(blank=True, null=True)
    timer = models.SmallIntegerField(blank=True, null=True)
    random = models.NullBooleanField()
    used = models.NullBooleanField()
    deleted = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 'override'





class UsersRooms(models.Model):
    working_user_id = models.AutoField(primary_key=True)
    user = models.ForeignKey(Users, models.DO_NOTHING, blank=True, null=True)
    first_name = models.CharField(max_length=100, blank=True, null=True)
    room_id = models.IntegerField()
    con_time = models.DateTimeField(blank=True, null=True)
    discon_time = models.DateTimeField(blank=True, null=True)
    second_name = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'users_rooms'


class Subjects(models.Model):
    subject_id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'subjects'




class Lessons(models.Model):
    lesson_id = models.AutoField(primary_key=True)
    room = models.ForeignKey(Rooms, models.DO_NOTHING)
    subject = models.ForeignKey(Subjects, models.DO_NOTHING, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    start_time = models.DateTimeField(blank=True, null=True)
    end_time = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'lessons'

class Streams(models.Model):
    stream_id = models.AutoField(primary_key=True)
    room = models.ForeignKey(Rooms, models.DO_NOTHING, blank=True, null=True)
    test = models.ForeignKey('Tests', models.DO_NOTHING)
    add_time = models.DateTimeField(blank=True, null=True)
    del_time = models.DateTimeField(blank=True, null=True)
    del_reason = models.TextField(blank=True, null=True)  # This field type is a guess.
    override = models.ForeignKey(Override, models.DO_NOTHING, blank=True, null=True)
    pace_mode = models.SmallIntegerField(blank=True, null=True)
    lesson = models.ForeignKey(Lessons, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'streams'

class Sessions(models.Model):
    session_id = models.AutoField(primary_key=True)
    stream = models.ForeignKey(Streams, models.DO_NOTHING)
    working_user = models.ForeignKey(UsersRooms, models.DO_NOTHING)
    begin_time = models.DateTimeField(blank=True, null=True)
    end_time = models.DateTimeField(blank=True, null=True)
    del_reason = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'sessions'

class Answers(models.Model):
    answer_id = models.AutoField(primary_key=True)
    question_v = models.ForeignKey('QuestionsV', models.DO_NOTHING, blank=True, null=True)
    answer_v = models.ForeignKey('AnswersV', models.DO_NOTHING, blank=True, null=True)
    deleted = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'answers'


class AnswersC(models.Model):
    answer_c_id = models.AutoField(primary_key=True)
    answer_text = models.TextField(blank=True, null=True)
    correct_answer = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 'answers_c'


class AnswersV(models.Model):
    answer_v_id = models.AutoField(primary_key=True)
    answer_c = models.ForeignKey(AnswersC, models.DO_NOTHING, blank=True, null=True)
    deleted = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'answers_v'

class Groups(models.Model):
    group_id = models.AutoField(primary_key=True)
    test = models.ForeignKey('Tests', models.DO_NOTHING, blank=True, null=True)
    group_v = models.ForeignKey('GroupsV', models.DO_NOTHING, blank=True, null=True)
    deleted = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'groups'


class GroupsC(models.Model):
    group_c_id = models.AutoField(primary_key=True)
    group_description = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'groups_c'


class GroupsV(models.Model):
    group_v_id = models.AutoField(primary_key=True)
    group_c = models.ForeignKey(GroupsC, models.DO_NOTHING, blank=True, null=True)
    deleted = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'groups_v'


class Questions(models.Model):
    question_id = models.AutoField(primary_key=True)
    group_v = models.ForeignKey(GroupsV, models.DO_NOTHING, blank=True, null=True)
    question_v = models.ForeignKey('QuestionsV', models.DO_NOTHING, blank=True, null=True)
    deleted = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'questions'


class QuestionsC(models.Model):
    question_c_id = models.AutoField(primary_key=True)
    question_text = models.TextField(blank=True, null=True)
    question_type = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'questions_c'


class QuestionsV(models.Model):
    question_v_id = models.AutoField(primary_key=True)
    question_c = models.ForeignKey(QuestionsC, models.DO_NOTHING, blank=True, null=True)
    deleted = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'questions_v'


class Results(models.Model):
    result_id = models.AutoField(primary_key=True)
    session = models.ForeignKey('Sessions', models.DO_NOTHING)
    question = models.ForeignKey(Questions, models.DO_NOTHING)
    answer = models.ForeignKey(Answers, models.DO_NOTHING, blank=True, null=True)
    test = models.ForeignKey('Tests', models.DO_NOTHING, blank=True, null=True)
    open_answer_text = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'results'


class ResultsTime(models.Model):
    time_id = models.AutoField(primary_key=True)
    session = models.ForeignKey('Sessions', models.DO_NOTHING)
    question = models.ForeignKey(Questions, models.DO_NOTHING)
    time = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'results_time'