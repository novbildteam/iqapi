import datetime
from bs4 import BeautifulSoup
from openpyxl import Workbook
from openpyxl.styles import Font, PatternFill, Alignment, NamedStyle, Border, Side
from copy import copy
import os.path

from iqAPI.settings import PATH_TO_REPORTS

class XLSXException(Exception):
    pass

class StreamReport:

    _users_data = None
    _questions_data = None
    _stream_data = None
    _test_content = None
    _style = {
        'questions': None,
        'users': None,
    }

    _path_to = PATH_TO_REPORTS

    def __init__(self, stream_data, users_data, questions_data):
        self._stream_data = stream_data
        self._users_data = users_data
        self._questions_data = questions_data
        self._test_content = questions_data['test_content']
        self._make_workbook()

    @property
    def data(self):
        return open(os.path.join(self._path_to, 's{}.xlsx'.format(self._stream_data['stream_id'])), mode='rb')

    def _make_workbook(self):
        if not self._users_data or not self._questions_data or not self._stream_data:
            raise XLSXException('no users_data, questions_data or stream_data')
        wb = Workbook()
        self._workbook = wb
        self._setup_style()
        ws = wb.active
        self._make_questions_worksheet(wb, ws)
        self._make_users_worksheet(wb)
        wb.save(os.path.join(self._path_to, 's{}.xlsx'.format(self._stream_data['stream_id'])))

    def _make_questions_worksheet(self, workbook, worksheet=None):
        if worksheet is None:
            ws = workbook.create_sheet('по вопросам')
        else:
            ws = worksheet
            ws.title = 'по вопросам'
        ws.row_dimensions[1].height = 50
        ws.row_dimensions[2].height = 50
        ws.column_dimensions['A'].width = 20
        ws.column_dimensions['B'].width = 20
        ws.column_dimensions['C'].width = 20
        ws.column_dimensions['D'].width = 20
        ws.column_dimensions['E'].width = 20

        test = self._stream_data['test']
        # activity headers
        activity_header_style = self._style['questions']['activity']['header']
        activity_item_style = self._style['questions']['activity']['item']
        ws['A1'] = 'Тип активности'
        ws['B1'] = 'Название'
        ws['C1'] = 'Вопросов'
        ws['D1'] = 'Дата проведения'
        for cell in ws['A1:E1'][0]:
            cell.style = activity_header_style

        if test['type'] == 0:
            ws['A2'] = 'Тест'
        elif test['type'] == 1:
            ws['A2'] = 'Опрос'
        ws['E1'] = 'Пользователей'
        ws['B2'] = test['name']
        ws['C2'] = test['questions']
        ws['D2'] = datetime.datetime.utcfromtimestamp(self._stream_data['add_time']).strftime('%d/%m/%Y %H:%M:%S')
        ws['E2'] = len(self._users_data['users'])
        for cell in ws['A2:E2'][0]:
            cell.style = activity_item_style
        row = 3
        rows = row
        # questions
        for group_i, group in enumerate(self._questions_data['groups']):
            rows += self._make_group(ws, rows, group, group_i+1)

    def _make_group(self, ws, row, group, count):
        ws.merge_cells(start_row=row, end_row=row, start_column=1, end_column=4)
        group_cell = ws.cell(row=row, column=1)
        group_cell.value = 'Вопрос №{}'.format(count)
        group_cell.style = self._style['questions']['question']['header']
        only_one_version = len(group['questions']) == 1
        rows = 1
        for question_i, question in enumerate(group['questions']):
            if not only_one_version:
                ws.merge_cells(start_row=row+rows, end_row=row+rows, start_column=1, end_column=4)
                option_cell = ws.cell(row=row+rows, column=1)
                option_cell.value = 'Вариант {}'.format(question_i+1)
                option_cell.style = self._style['questions']['question']['option']
                rows += 1
            rows += self._make_question(ws, row+rows, question)
        return rows

    def _make_question(self, ws, row, question):
        question_text = self._test_content['questions'][question['question_id']]['text']
        question_html = BeautifulSoup(question_text, 'html.parser')
        question_text = question_html.get_text()
        question_img = question_html.find('img')
        if question_img:
            question_img = question_img['src']
            question_text += '\n'+question_img
        ws.merge_cells(start_row=row, end_row=row, start_column=1, end_column=3)
        ws.cell(row=row, column=1).value = question_text
        type_cell = ws.cell(row=row, column=4)
        type_cell.alignment = Alignment(horizontal='center', vertical='center')
        if question['type'] == 0 or question['type'] == 1:
            type_cell.value = 'закрытый'
        elif question['type'] == 2:
            type_cell.value = 'открытый'
        # header
        header_style = self._style['questions']['answers']['header']
        cell = ws.cell(row=row+1, column=1)
        cell.value = 'Ответы'
        cell.style = header_style
        cell = ws.cell(row=row+1, column=2)
        cell.value = 'Ответов, шт.'
        cell.style = header_style
        cell= ws.cell(row=row+1, column=3)
        cell.value = 'Ответов, %'
        cell.style = header_style
        cell = ws.cell(row=row+1, column=4)
        cell.value = 'Правильный?'
        cell.style = header_style
        rows = 2
        for answer_i, answer in enumerate(question['answers']):
            rows += self._make_answer(ws, row+rows, answer, question['count'])
        footer_style = self._style['questions']['question']['footer']
        cell = ws.cell(row=row+rows, column=1)
        cell.value = 'Итого:'
        cell.style = footer_style
        cell = ws.cell(row=row+rows, column=2)
        cell.value = question['count']
        cell.style = footer_style
        cell = ws.cell(row=row+rows, column=3)
        if 'correct_rate' in question:
            cell.value = '{}%'.format(int(question['correct_rate']*100))
        else:
            cell.value = '-'
        cell.style = footer_style
        cell.alignment = Alignment(horizontal='right')
        cell = ws.cell(row=row+rows, column=4)
        if 'all_correct_answers' in question:
            cell.value = question['all_correct_answers']
        else:
            cell.value = '-'
        cell.style = footer_style
        rows += 1
        return rows

    def _make_user_question(self, ws, row, question):
        ws.merge_cells(start_row=row, end_row=row, start_column=1, end_column=4)
        header_cell = ws.cell(row=row, column=1)
        header_cell.value = 'Вопрос {}'.format(question['position'])
        header_cell.style = self._style['users']['question']['header']
        question_text = self._test_content['questions'][question['question_id']]['text']
        question_html = BeautifulSoup(question_text, 'html.parser')
        question_text = question_html.get_text()
        question_img = question_html.find('img')
        if question_img:
            question_img = question_img['src']
            question_text += '\n' + question_img
        ws.merge_cells(start_row=row+1, end_row=row+1, start_column=1, end_column=3)
        ws.cell(row=row+1, column=1).value = question_text
        type_cell = ws.cell(row=row+1, column=4)
        type_cell.alignment = Alignment(horizontal='center', vertical='center')
        if question['type'] == 0 or question['type'] == 1:
            type_cell.value = 'закрытый'
        elif question['type'] == 2:
            type_cell.value = 'открытый'
        # header
        header_style = self._style['users']['answers']['header']
        ws.merge_cells(start_row=row+2, end_row=row+2, start_column=1, end_column=3)
        cell = ws.cell(row=row + 2, column=1)
        cell.value = 'Ответы'
        cell.style = header_style
        cell = ws.cell(row=row + 2, column=4)
        cell.value = 'Правильный?'
        cell.style = header_style
        rows = 3
        for answer_i, answer in enumerate(question['answers']):
            rows += self._make_user_answer(ws, row + rows, answer)
        footer_style = self._style['users']['question']['footer']
        return rows

    def _make_answer(self, ws, row, answer, question_count):
        answer_text = answer.get('text')
        if answer_text is None:
            answer_text = self._test_content['answers'][answer['answer_id']]['text']

        item_style = self._style['questions']['answers']['answer_item']
        correct_item_style = self._style['questions']['answers']['correct_answer_item']


        text_cell = ws.cell(row=row, column=1)
        text_cell.value = answer_text
        text_cell.style = item_style

        count_cell = ws.cell(row=row, column=2)
        count_cell.value = answer['count']
        count_cell.style = item_style

        percent_cell = ws.cell(row=row, column=3)
        percent_cell.style = item_style

        correct_cell = ws.cell(row=row, column=4)
        correct_cell.style = item_style
        if not 'correct' in answer:
            correct = '-'
        else:
            if answer['correct'] is True:
                correct_cell.style = correct_item_style
                text_cell.style = correct_item_style
                count_cell.style = correct_item_style
                percent_cell.style = correct_item_style
                correct = 'да'
            else:
                correct = 'нет'
        correct_cell.value = correct

        if question_count > 0:
            percent_cell.value = '{}%'.format(int(answer['count']/question_count*100))
            percent_cell.alignment = Alignment(horizontal='right')
        else:
            percent_cell.value = ''

        return 1

    def _make_user_answer(self, ws, row, answer):
        answer_text = answer.get('text')
        if answer_text is None:
            answer_text = self._test_content['answers'][answer['answer_id']]['text']

        item_style = self._style['users']['answers']['answer_item']
        correct_item_style = self._style['users']['answers']['correct_answer_item']
        incorrect_item_style = self._style['users']['answers']['incorrect_answer_item']
        ws.merge_cells(start_row=row, end_row=row, start_column=1, end_column=3)
        text_cell = ws.cell(row=row, column=1)
        text_cell.value = answer_text
        text_cell.style = item_style

        correct_cell = ws.cell(row=row, column=4)
        correct_cell.style = item_style
        if not 'correct_answer' in answer:
            correct = '-'
        else:
            if answer['correct_answer'] is True:
                correct_cell.style = correct_item_style
                text_cell.style = correct_item_style
                correct = 'да'
            else:
                correct_cell.style = incorrect_item_style
                text_cell.style = incorrect_item_style
                correct = 'нет'
        correct_cell.value = correct

        return 1

    def _make_users_worksheet(self, workbook, worksheet=None):
        if not worksheet:
            ws = workbook.create_sheet('по пользователям')
        else:
            ws = worksheet
        ws.row_dimensions[1].height = 50
        ws.row_dimensions[2].height = 50
        ws.column_dimensions['A'].width = 20
        ws.column_dimensions['B'].width = 20
        ws.column_dimensions['C'].width = 20
        ws.column_dimensions['D'].width = 20
        ws.column_dimensions['E'].width = 20
        ws.column_dimensions['E'].width = 20

        test = self._stream_data['test']
        # activity headers
        activity_header_style = self._style['users']['activity']['header']
        activity_item_style = self._style['users']['activity']['item']
        ws['A1'] = 'Тип активности'
        ws['B1'] = 'Название'
        ws['C1'] = 'Вопросов'
        ws['D1'] = 'Дата проведения'
        ws['E1'] = 'Пользователей'
        for cell in ws['A1:E1'][0]:
            cell.style = activity_header_style

        if test['type'] == 0:
            ws['A2'] = 'Тест'
        elif test['type'] == 1:
            ws['A2'] = 'Опрос'
        ws['B2'] = test['name']
        ws['C2'] = test['questions']
        ws['D2'] = datetime.datetime.utcfromtimestamp(self._stream_data['add_time']).strftime('%d/%m/%Y %H:%M:%S')
        ws['E2'] = len(self._users_data['users'])
        for cell in ws['A2:E2'][0]:
            cell.style = activity_item_style
        row = 3
        rows = 0
        for user in self._users_data['users']:
            rows += self._make_user(ws, row+rows, user) + 2

    def _make_user(self, ws, row, user):
        ws.row_dimensions[row].height = 30
        ws.row_dimensions[row].height = 30
        ws.row_dimensions[row+1].height = 30
        ws.row_dimensions[row+1].height = 30
        # header
        header_style = self._style['users']['user']['header']
        cell = ws.cell(row=row, column=1)
        cell.value = 'ФИО'
        cell.style = header_style
        cell = ws.cell(row=row, column=2)
        cell.value = 'id'
        cell.style = header_style
        cell = ws.cell(row=row, column=3)
        cell.value = 'Правильных, шт.'
        cell.style = header_style
        cell = ws.cell(row=row, column=4)
        cell.value = 'Правильных, %'
        cell.style = header_style
        cell = ws.cell(row=row, column=5)
        cell.value = 'Длительность прохождения'
        cell.style = header_style
        alg = copy(cell.alignment)
        alg.wrap_text = True
        cell.alignment = alg
        item_style = self._style['users']['user']['item']
        cell = ws.cell(row=row+1, column=1)
        cell.value = ''
        if user.get('first_name'):
            cell.value += user.get('first_name')
        if user.get('second_name'):
            cell.value += user.get('second_name')
        cell.style = item_style
        cell = ws.cell(row=row+1, column=2)
        cell.value = user.get('user_id') or '-'
        cell.style = item_style
        cell = ws.cell(row=row+1, column=3)
        cell.style = item_style
        if self._stream_data['test']['type'] == 0:
            all_correct_answers = self._count_all_correct_answers(user)
            cell.value = all_correct_answers
            cell = ws.cell(row=row+1, column=4)
            cell.value = '{}%'.format(int(all_correct_answers / self._stream_data['test']['questions'] * 100))
            cell.style = item_style
        elif self._stream_data['test']['type'] == 1:
            all_answers = len(user['questions'])
            cell.value = all_answers
            cell = ws.cell(row=row+1, column=4)
            cell.value = '-'
            cell.style = item_style

        cell = ws.cell(row=row+1, column=5)
        cell.value = '{} ч {} мин {} сек'.format(*self._get_time_from_ms(user['time']*10))
        cell.style = item_style
        # questions
        rows = 2
        for question_i, question in enumerate(user['questions']):
            rows += self._make_user_question(ws, row+rows, question)

        return rows

    def _count_all_correct_answers(self, user):
        all_correct_answers = sum([1 for q in user['questions'] if q['correct_answers'] == q['all_correct_answers']])
        return all_correct_answers

    def _get_time_from_ms(self, milliseconds):
        t = datetime.timedelta(milliseconds=milliseconds)
        hours, remainder = divmod(t.seconds, 3600)
        minutes, seconds = divmod(remainder, 60)
        return hours, minutes, seconds

    def _setup_style(self):

        # questions worksheet styles
        default_border = Border(left=Side(border_style='thin'),
                                right=Side(border_style='thin'),
                                top=Side(border_style='thin'),
                                bottom=Side(border_style='thin'))

        answer_item_style = NamedStyle(name='q_answer_item_style',
                                       font=Font(name='Calibri', sz=10),
                                       border=default_border)
        self._workbook.add_named_style(answer_item_style)
        answer_header_style = NamedStyle(name='q_answer_header_style',
                                           font=Font(name='Calibri', sz=10, bold=False),
                                           fill=PatternFill('solid', start_color='EEEEEE'),
                                           alignment=Alignment(horizontal='center', vertical='center'),
                                           border=default_border)
        self._workbook.add_named_style(answer_header_style)
        correct_answer_item_style = NamedStyle(name='q_correct_answer_item_style',
                                               font=Font(name='Calibri', sz=10),
                                               fill=PatternFill(fill_type='solid', start_color='99FF66'),
                                               border=default_border)
        self._workbook.add_named_style(correct_answer_item_style)
        activity_header_style = NamedStyle(name='q_activity_header_style',
                                           font=Font(name='Calibri', sz=10, bold=True),
                                           alignment=Alignment(horizontal='center',
                                                               vertical='center',
                                                               wrap_text=True,
                                                               indent=100),
                                           border=default_border)
        self._workbook.add_named_style(activity_header_style)
        activity_item_style = NamedStyle(name='q_acitivity_item_style',
                                         font=Font(name='Calibri', sz=10),
                                         alignment=Alignment(horizontal='center',
                                                             vertical='center',
                                                             wrap_text=True,
                                                             indent=10),
                                         border=default_border)
        self._workbook.add_named_style(activity_item_style)
        question_header_style = NamedStyle(name='q_question_header_style',
                                           font=Font(name='Calibri', sz=10, bold=True),
                                           fill=PatternFill('solid', start_color='CCCCCC'),
                                           alignment=Alignment(horizontal='center', vertical='center'),
                                           border=default_border)
        self._workbook.add_named_style(question_header_style)
        question_item_style = NamedStyle(name='q_question_item_style',
                                         font=Font(name='Calibri', sz=10, bold=True),
                                         border=default_border)
        self._workbook.add_named_style(question_item_style)
        question_option_style = NamedStyle(name='q_question_option_style',
                                           font=Font(name='Calibri', sz=10),
                                           border=default_border)
        self._workbook.add_named_style(question_option_style)
        question_footer_style = NamedStyle(name='q_question_footer_style',
                                           font=Font(name='Calibri', sz=10, bold=True),
                                           border=default_border)
        self._workbook.add_named_style(question_footer_style)
        answer_incorrect_style = NamedStyle(name='q_incorrect_answer_item_style',
                                               font=Font(name='Calibri', sz=10),
                                               fill=PatternFill(fill_type='solid', start_color='FF6666'),
                                               border=default_border)
        self._workbook.add_named_style(answer_incorrect_style)
        user_question_header_style = NamedStyle(name='usr_question_header_style',
                                                font=Font(name='Calibri', sz=10, bold=True),
                                                fill=PatternFill('solid', start_color='EEEEEE'),
                                                alignment=Alignment(horizontal='center', vertical='center'),
                                                border=default_border)
        self._workbook.add_named_style(user_question_header_style)

        self._style['questions'] = {
            'answers': {
                'header': answer_header_style,
                'answer_item': answer_item_style,
                'correct_answer_item': correct_answer_item_style,
            },
            'activity': {
                'header': activity_header_style,
                'item': activity_item_style,
            },
            'question': {
                'header': question_header_style,
                'item': question_item_style,
                'option': question_option_style,
                'footer': question_footer_style,
            }
        }
        self._style['users'] = {
            'activity': {
                'header': activity_header_style,
                'item': activity_item_style,
            },
            'user': {
                'header': question_header_style,
                'item': activity_item_style,
            },
            'answers': {
                'header': answer_header_style,
                'answer_item': answer_item_style,
                'correct_answer_item': correct_answer_item_style,
                'incorrect_answer_item': answer_incorrect_style,
            },
            'question': {
                'header': user_question_header_style,
                'item': question_item_style,
                'option': question_option_style,
                'footer': question_footer_style,
            }
        }

if __name__ == '__main__':

    stream = {
        "stream_id": 1124,
        "test": {
            "owner_id": 3,
            "branch_id": 153,
            "name": "Сложение и вычитание в различных СС",
            "type": 0,
            "questions": 6,
            "timer": 120,
            "random": 0,
            "description": "",
            "test_id": 231
        },
        "override": {
            "random": None,
            "timer": 240
        },
        "add_time": 1474105924,
        "del_time": 1474106681,
        "del_reason": "finish",
        "pace_mode": 1,
        "lesson_id": 305
    }
    questions_data = {
      "stream_id": 1124,
      "groups": [
        {
          "questions": [
            {
              "question_id": 432,
              "all_correct_answers": 1,
              "answers": [
                {
                  "count": 0,
                  "correct": True,
                  "text": "1100100",
                  "answer_id": 1055
                },
                {
                  "correct": True,
                  "text": "1100100",
                  "count": 8
                },
                {
                  "correct": False,
                  "text": "2",
                  "count": 1
                },
                {
                  "correct": True,
                  "text": "1010100",
                  "count": 1
                }
              ],
              "count": 10,
              "correct_answers": 8,
              "correct_rate": 0.8,
              "type": 2
            },
            {
              "question_id": 433,
              "all_correct_answers": 1,
              "answers": [
                {
                  "count": 0,
                  "correct": True,
                  "text": "1100100",
                  "answer_id": 1056
                },
                {
                  "correct": True,
                  "text": "1100000",
                  "count": 5
                }
              ],
              "count": 5,
              "correct_answers": 0,
              "correct_rate": 0,
              "type": 2
            },
            {
              "question_id": 434,
              "all_correct_answers": 1,
              "answers": [
                {
                  "count": 0,
                  "correct": True,
                  "text": "110000",
                  "answer_id": 1057
                },
                {
                  "correct": True,
                  "text": "1110000",
                  "count": 6
                }
              ],
              "count": 6,
              "correct_answers": 0,
              "correct_rate": 0,
              "type": 2
            }
          ],
          "correct_answers": 8,
          "count": 21,
          "correct_rate": 0.38095238095238093,
          "position": 1,
          "group_id": 635
        },
        {
          "questions": [
            {
              "question_id": 435,
              "all_correct_answers": 1,
              "answers": [
                {
                  "count": 0,
                  "correct": True,
                  "text": "110",
                  "answer_id": 1058
                },
                {
                  "correct": True,
                  "text": "110",
                  "count": 2
                },
                {
                  "correct": True,
                  "text": "000000",
                  "count": 1
                },
                {
                  "correct": True,
                  "text": "1000",
                  "count": 1
                }
              ],
              "count": 4,
              "correct_answers": 2,
              "correct_rate": 0.5,
              "type": 2
            },
            {
              "question_id": 436,
              "all_correct_answers": 1,
              "answers": [
                {
                  "count": 0,
                  "correct": True,
                  "text": "-110",
                  "answer_id": 1059
                },
                {
                  "correct": True,
                  "text": "-110",
                  "count": 3
                },
                {
                  "correct": True,
                  "text": "-10110",
                  "count": 1
                },
                {
                  "correct": True,
                  "text": "000110",
                  "count": 1
                },
                {
                  "correct": True,
                  "text": "110",
                  "count": 1
                },
                {
                  "correct": True,
                  "text": "11000",
                  "count": 1
                },
                {
                  "correct": True,
                  "text": "-101000",
                  "count": 1
                }
              ],
              "count": 8,
              "correct_answers": 3,
              "correct_rate": 0.375,
              "type": 2
            },
            {
              "question_id": 437,
              "all_correct_answers": 1,
              "answers": [
                {
                  "count": 0,
                  "correct": True,
                  "text": "-110",
                  "answer_id": 1060
                },
                {
                  "correct": True,
                  "text": "-110",
                  "count": 3
                },
                {
                  "correct": True,
                  "text": "11010",
                  "count": 1
                },
                {
                  "correct": True,
                  "text": "0110",
                  "count": 1
                },
                {
                  "correct": True,
                  "text": "10010",
                  "count": 2
                },
                {
                  "correct": True,
                  "text": "110",
                  "count": 1
                }
              ],
              "count": 8,
              "correct_answers": 3,
              "correct_rate": 0.375,
              "type": 2
            }
          ],
          "correct_answers": 8,
          "count": 20,
          "correct_rate": 0.4,
          "position": 2,
          "group_id": 636
        },
        {
          "questions": [
            {
              "question_id": 445,
              "all_correct_answers": 1,
              "answers": [
                {
                  "count": 0,
                  "correct": True,
                  "text": "1012",
                  "answer_id": 1068
                },
                {
                  "correct": True,
                  "text": "1021",
                  "count": 1
                }
              ],
              "count": 1,
              "correct_answers": 0,
              "correct_rate": 0,
              "type": 2
            },
            {
              "question_id": 446,
              "all_correct_answers": 1,
              "answers": [
                {
                  "count": 0,
                  "correct": True,
                  "text": "1011",
                  "answer_id": 1069
                },
                {
                  "correct": True,
                  "text": "1122",
                  "count": 1
                },
                {
                  "correct": True,
                  "text": "1012",
                  "count": 1
                },
                {
                  "correct": True,
                  "text": "1020",
                  "count": 1
                },
                {
                  "correct": True,
                  "text": "1011",
                  "count": 5
                }
              ],
              "count": 8,
              "correct_answers": 5,
              "correct_rate": 0.625,
              "type": 2
            },
            {
              "question_id": 447,
              "all_correct_answers": 1,
              "answers": [
                {
                  "count": 0,
                  "correct": True,
                  "text": "1040",
                  "answer_id": 1070
                },
                {
                  "correct": True,
                  "text": "1040",
                  "count": 3
                },
                {
                  "correct": True,
                  "text": "1050",
                  "count": 2
                }
              ],
              "count": 5,
              "correct_answers": 3,
              "correct_rate": 0.6,
              "type": 2
            }
          ],
          "correct_answers": 8,
          "count": 14,
          "correct_rate": 0.5714285714285714,
          "position": 3,
          "group_id": 639
        },
        {
          "questions": [
            {
              "question_id": 448,
              "all_correct_answers": 1,
              "answers": [
                {
                  "count": 0,
                  "correct": True,
                  "text": "-134",
                  "answer_id": 1071
                },
                {
                  "correct": True,
                  "text": "-123",
                  "count": 2
                },
                {
                  "correct": True,
                  "text": "-134",
                  "count": 1
                }
              ],
              "count": 3,
              "correct_answers": 1,
              "correct_rate": 0.3333333333333333,
              "type": 2
            },
            {
              "question_id": 449,
              "all_correct_answers": 1,
              "answers": [
                {
                  "count": 0,
                  "correct": True,
                  "text": "-117",
                  "answer_id": 1072
                },
                {
                  "correct": True,
                  "text": "-151",
                  "count": 1
                },
                {
                  "correct": True,
                  "text": "-117",
                  "count": 2
                },
                {
                  "correct": True,
                  "text": "-11",
                  "count": 1
                }
              ],
              "count": 4,
              "correct_answers": 2,
              "correct_rate": 0.5,
              "type": 2
            },
            {
              "question_id": 450,
              "all_correct_answers": 1,
              "answers": [
                {
                  "count": 0,
                  "correct": True,
                  "text": "-306",
                  "answer_id": 1073
                },
                {
                  "correct": True,
                  "text": "306",
                  "count": 4
                },
                {
                  "correct": True,
                  "text": "336",
                  "count": 1
                }
              ],
              "count": 5,
              "correct_answers": 0,
              "correct_rate": 0,
              "type": 2
            }
          ],
          "correct_answers": 3,
          "count": 12,
          "correct_rate": 0.25,
          "position": 4,
          "group_id": 640
        },
        {
          "questions": [
            {
              "question_id": 452,
              "all_correct_answers": 2,
              "answers": [
                {
                  "count": 0,
                  "correct": True,
                  "text": "12a",
                  "answer_id": 1075
                },
                {
                  "count": 0,
                  "correct": True,
                  "text": "12A",
                  "answer_id": 1078
                },
                {
                  "correct": True,
                  "text": "1CA",
                  "count": 1
                }
              ],
              "count": 1,
              "correct_answers": 0,
              "correct_rate": 0,
              "type": 2
            },
            {
              "question_id": 455,
              "all_correct_answers": 2,
              "answers": [
                {
                  "count": 0,
                  "correct": True,
                  "text": "D2",
                  "answer_id": 1079
                },
                {
                  "count": 0,
                  "correct": True,
                  "text": "d2",
                  "answer_id": 1080
                },
                {
                  "correct": True,
                  "text": "D1",
                  "count": 1
                },
                {
                  "correct": True,
                  "text": "d2",
                  "count": 1
                },
                {
                  "correct": True,
                  "text": "D4",
                  "count": 1
                },
                {
                  "correct": True,
                  "text": "E4",
                  "count": 1
                },
                {
                  "correct": True,
                  "text": "D2",
                  "count": 1
                }
              ],
              "count": 5,
              "correct_answers": 0,
              "correct_rate": 0,
              "type": 2
            },
            {
              "question_id": 456,
              "all_correct_answers": 2,
              "answers": [
                {
                  "count": 0,
                  "correct": True,
                  "text": "D3",
                  "answer_id": 1081
                },
                {
                  "count": 0,
                  "correct": True,
                  "text": "d3",
                  "answer_id": 1082
                },
                {
                  "correct": True,
                  "text": "D3",
                  "count": 3
                }
              ],
              "count": 3,
              "correct_answers": 0,
              "correct_rate": 0,
              "type": 2
            }
          ],
          "correct_answers": 0,
          "count": 9,
          "correct_rate": 0,
          "position": 5,
          "group_id": 642
        },
        {
          "questions": [
            {
              "question_id": 457,
              "all_correct_answers": 2,
              "answers": [
                {
                  "count": 0,
                  "correct": True,
                  "text": "-1b",
                  "answer_id": 1083
                },
                {
                  "count": 0,
                  "correct": True,
                  "text": "-1B",
                  "answer_id": 1084
                },
                {
                  "correct": True,
                  "text": "D0",
                  "count": 1
                }
              ],
              "count": 1,
              "correct_answers": 0,
              "correct_rate": 0,
              "type": 2
            },
            {
              "question_id": 458,
              "all_correct_answers": 2,
              "answers": [
                {
                  "count": 0,
                  "correct": True,
                  "text": "-5e",
                  "answer_id": 1085
                },
                {
                  "count": 0,
                  "correct": True,
                  "text": "-5E",
                  "answer_id": 1086
                },
                {
                  "correct": True,
                  "text": "6A",
                  "count": 1
                },
                {
                  "correct": True,
                  "text": "-5C",
                  "count": 1
                }
              ],
              "count": 2,
              "correct_answers": 0,
              "correct_rate": 0,
              "type": 2
            },
            {
              "question_id": 459,
              "all_correct_answers": 2,
              "answers": [
                {
                  "count": 0,
                  "correct": True,
                  "text": "-3D",
                  "answer_id": 1087
                },
                {
                  "count": 0,
                  "correct": True,
                  "text": "-3d",
                  "answer_id": 1088
                },
                {
                  "correct": True,
                  "text": "-3D",
                  "count": 2
                }
              ],
              "count": 2,
              "correct_answers": 0,
              "correct_rate": 0,
              "type": 2
            }
          ],
          "correct_answers": 0,
          "count": 5,
          "correct_rate": 0,
          "position": 6,
          "group_id": 643
        }
      ],
      "test_content": {
        "questions": {
          432: {
            "text": "<p>вычислите (систему счисления в ответе не указывайте)</p><p><span>110101<sub>b</sub> + 101111<sub>b</sub></span><br/></p>"
          },
          433: {
            "text": "<p>вычислите (систему счисления в ответе не указывайте)</p><p><!--startfragment--><span style=\"color: rgb(85, 85, 85);float: none;background-color: rgb(255, 255, 255);\">101101<sub>b </sub> <span style=\"color: rgb(85, 85, 85);float: none;background-color: rgb(255, 255, 255);\">+ 110011<sub>b</sub></span><!--endfragment--><br/><br/></span><br/></p>"
          },
          434: {
            "text": "<p style=\"color: rgb(85, 85, 85);background-color: rgb(255, 255, 255);\">вычислите (систему счисления в ответе не указывайте)</p><p style=\"color: rgb(85, 85, 85);background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(85, 85, 85);float: none;background-color: rgb(255, 255, 255);\">110101<sub>b<span class=\"apple-converted-space\"> </span></sub> <span style=\"color: rgb(85, 85, 85);float: none;background-color: rgb(255, 255, 255);\">+ 111011<sub>b</sub></span></span></p>"
          },
          435: {
            "text": "<p>вычислите (систему счисления в ответе не указывайте)</p><p><span>110101<sub>b</sub> - 101111<sub>b</sub></span></p><p><br/></p><p></p>"
          },
          436: {
            "text": "<p>вычислите (систему счисления в ответе не указывайте)</p><p><span style=\"color: rgb(85, 85, 85);float: none;background-color: rgb(255, 255, 255);\">101101<sub>b </sub> <span style=\"color: rgb(85, 85, 85);float: none;background-color: rgb(255, 255, 255);\">- 110011<sub>b</sub></span><!--endfragment--><br/><br/></span></p><p><br/></p><p><!--startfragment--></p>"
          },
          437: {
            "text": "<p style=\"color: rgb(85, 85, 85);background-color: rgb(255, 255, 255);\">вычислите (систему счисления в ответе не указывайте)</p><p><span style=\"color: rgb(85, 85, 85);float: none;background-color: rgb(255, 255, 255);\">110101<sub>b<span class=\"apple-converted-space\"> </span></sub> <span style=\"color: rgb(85, 85, 85);float: none;background-color: rgb(255, 255, 255);\">- 111011<sub>b</sub></span></span></p><p style=\"color: rgb(85, 85, 85);background-color: rgb(255, 255, 255);\"></p><p><br/></p><p></p><p><br/></p><p></p>"
          },
          445: {
            "text": "<p>вычислите (систему счисления в ответе не указывайте)</p><p><span>423<sub>o</sub> + 367<sub>o</sub></span></p><p></p><p></p><p></p><p></p><p><br/></p><p></p><p></p><p></p><p></p><p></p>"
          },
          446: {
            "text": "<p>вычислите (систему счисления в ответе не указывайте)</p><p><span>314<sub>o</sub> + 475<sub>o</sub></span></p>"
          },
          447: {
            "text": "<p>вычислите (систему счисления в ответе не указывайте)</p><p><span>563<sub>o</sub> + 255<sub>o</sub></span></p><p></p><p></p><p></p><p></p><p></p><p> </p><p></p><p></p><p></p><p></p><p></p><p></p>"
          },
          448: {
            "text": "<p>вычислите (систему счисления в ответе не указывайте)</p><p><span>167<sub>o</sub> - 323<sub>o</sub></span></p><p></p><p></p><p></p><p></p><p><br/></p><p></p><p></p><p></p><p></p><p></p>"
          },
          449: {
            "text": "<p>вычислите (систему счисления в ответе не указывайте)</p><p><span>375<sub>o</sub> - 514<sub>o</sub></span></p>"
          },
          450: {
            "text": "<p>вычислите (систему счисления в ответе не указывайте)</p><p><span>563<sub>o</sub> - 255<sub>o</sub></span></p><p></p><p></p><p></p><p></p><p></p><p><br/></p><p></p><p></p><p></p><p></p><p></p><p></p>"
          },
          452: {
            "text": "<p>вычислите (систему счисления в ответе не указывайте)</p><p><span>A3<sub>h</sub> + 87<sub>h</sub></span></p><p></p><p></p><p></p><p></p><p><br/></p><p></p><p></p><p></p><p></p><p></p>"
          },
          455: {
            "text": "<p>вычислите (систему счисления в ответе не указывайте)</p><p><span>9B<sub>h</sub> + 37<sub>h</sub></span></p><p></p><p></p><p></p><p></p><p><br/></p><p></p><p></p><p></p><p></p><p></p>"
          },
          456: {
            "text": "<p>вычислите (систему счисления в ответе не указывайте)</p><p><span>8A<sub>h</sub> + 49<sub>h</sub></span></p><p></p><p></p><p></p><p></p><p><br/></p><p></p><p></p><p></p><p></p><p></p>"
          },
          457: {
            "text": "<p>вычислите (систему счисления в ответе не указывайте)</p><p>87<sub>h</sub> - A2<sub>h</sub></p><p></p><p></p><p></p><p></p><p><br/></p><p></p><p></p><p></p><p></p><p></p>"
          },
          458: {
            "text": "<p>вычислите (систему счисления в ответе не указывайте)</p><p>3B<sub>h</sub> - 97<sub>h</sub></p><p></p><p></p><p></p><p></p><p><br/></p><p></p><p></p><p></p><p></p><p></p>"
          },
          459: {
            "text": "<p>вычислите (систему счисления в ответе не указывайте)</p><p>4C<sub>h</sub> - 89<sub>h</sub></p><p></p><p></p><p></p><p></p><p><br/></p><p></p><p></p><p></p><p></p><p></p>"
          }
        },
        "answers": {
          1055: {
            "text": "1100100"
          },
          1056: {
            "text": "1100100"
          },
          1057: {
            "text": "110000"
          },
          1058: {
            "text": "110"
          },
          1059: {
            "text": "-110"
          },
          1060: {
            "text": "-110"
          },
          1068: {
            "text": "1012"
          },
          1069: {
            "text": "1011"
          },
          1070: {
            "text": "1040"
          },
          1071: {
            "text": "-134"
          },
          1072: {
            "text": "-117"
          },
          1073: {
            "text": "-306"
          },
          1075: {
            "text": "12a"
          },
          1078: {
            "text": "12A"
          },
          1079: {
            "text": "D2"
          },
          1080: {
            "text": "d2"
          },
          1081: {
            "text": "D3"
          },
          1082: {
            "text": "d3"
          },
          1083: {
            "text": "-1b"
          },
          1084: {
            "text": "-1B"
          },
          1085: {
            "text": "-5e"
          },
          1086: {
            "text": "-5E"
          },
          1087: {
            "text": "-3D"
          },
          1088: {
            "text": "-3d"
          }
        }
      }
    }
    users_data = {
  "stream_id": 1124,
  "users": [
    {
      "workingUser_id": 5143,
      "first_name": "Семен Орехов",
      "second_name": None,
      "user_id": None,
      "del_reason": "finish",
      "begin_time": 1474106076,
      "end_time": 1474106315,
      "session_id": 3148,
      "time": 18212,
      "questions": [
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 3074,
          "question_id": 433,
          "position": 1,
          "group_id": 635,
          "answers": [
            {
              "answer_id": None,
              "text": "1100000",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 3300,
          "question_id": 436,
          "position": 2,
          "group_id": 636,
          "answers": [
            {
              "answer_id": None,
              "text": "-110",
              "correct_answer": True
            }
          ]
        },
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 3379,
          "question_id": 447,
          "position": 3,
          "group_id": 639,
          "answers": [
            {
              "answer_id": None,
              "text": "1040",
              "correct_answer": True
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 1845,
          "question_id": 450,
          "position": 4,
          "group_id": 640,
          "answers": [
            {
              "answer_id": None,
              "text": "306",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 4498,
          "question_id": 455,
          "position": 5,
          "group_id": 642,
          "answers": [
            {
              "answer_id": None,
              "text": "D4",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 2116,
          "question_id": 458,
          "position": 6,
          "group_id": 643,
          "answers": [
            {
              "answer_id": None,
              "text": "-5C",
              "correct_answer": False
            }
          ]
        }
      ]
    },
    {
      "workingUser_id": 5121,
      "first_name": "Dmitrij",
      "second_name": None,
      "user_id": None,
      "del_reason": "finish",
      "begin_time": 1474106034,
      "end_time": 1474106343,
      "session_id": 3126,
      "time": 24000,
      "questions": [
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 5315,
          "question_id": 434,
          "position": 1,
          "group_id": 635,
          "answers": [
            {
              "answer_id": None,
              "text": "1110000",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 8759,
          "question_id": 436,
          "position": 2,
          "group_id": 636,
          "answers": [
            {
              "answer_id": None,
              "text": "-110",
              "correct_answer": True
            }
          ]
        },
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 6124,
          "question_id": 446,
          "position": 3,
          "group_id": 639,
          "answers": [
            {
              "answer_id": None,
              "text": "1011",
              "correct_answer": True
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 3802,
          "question_id": 449,
          "position": 4,
          "group_id": 640,
          "answers": [
            {
              "answer_id": None,
              "text": "-11",
              "correct_answer": False
            }
          ]
        }
      ]
    },
    {
      "workingUser_id": 5116,
      "first_name": "Манас",
      "second_name": None,
      "user_id": None,
      "del_reason": "finish",
      "begin_time": 1474106033,
      "end_time": 1474106353,
      "session_id": 3121,
      "time": 24000,
      "questions": [
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 3219,
          "question_id": 433,
          "position": 1,
          "group_id": 635,
          "answers": [
            {
              "answer_id": None,
              "text": "1100000",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 3977,
          "question_id": 435,
          "position": 2,
          "group_id": 636,
          "answers": [
            {
              "answer_id": None,
              "text": "110",
              "correct_answer": True
            }
          ]
        },
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 4675,
          "question_id": 446,
          "position": 3,
          "group_id": 639,
          "answers": [
            {
              "answer_id": None,
              "text": "1011",
              "correct_answer": True
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 3988,
          "question_id": 449,
          "position": 4,
          "group_id": 640,
          "answers": [
            {
              "answer_id": None,
              "text": "-151",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 7808,
          "question_id": 455,
          "position": 5,
          "group_id": 642,
          "answers": [
            {
              "answer_id": None,
              "text": "E4",
              "correct_answer": False
            }
          ]
        }
      ]
    },
    {
      "workingUser_id": 5144,
      "first_name": "Смелкова Полина",
      "second_name": None,
      "user_id": None,
      "del_reason": "finish",
      "begin_time": 1474106098,
      "end_time": 1474106368,
      "session_id": 3149,
      "time": 21525,
      "questions": [
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 11338,
          "question_id": 432,
          "position": 1,
          "group_id": 635,
          "answers": [
            {
              "answer_id": None,
              "text": "1100100",
              "correct_answer": True
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 10187,
          "question_id": 437,
          "position": 2,
          "group_id": 636,
          "answers": [
            {
              "answer_id": None,
              "text": "10010",
              "correct_answer": False
            }
          ]
        }
      ]
    },
    {
      "workingUser_id": 5145,
      "first_name": "Артём",
      "second_name": None,
      "user_id": None,
      "del_reason": "finish",
      "begin_time": 1474106127,
      "end_time": 1474106371,
      "session_id": 3150,
      "time": 24000,
      "questions": [
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 9721,
          "question_id": 432,
          "position": 1,
          "group_id": 635,
          "answers": [
            {
              "answer_id": None,
              "text": "1100100",
              "correct_answer": True
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 14279,
          "question_id": 436,
          "position": 2,
          "group_id": 636,
          "answers": [
            {
              "answer_id": None,
              "text": "-101000",
              "correct_answer": False
            }
          ]
        }
      ]
    },
    {
      "workingUser_id": 5142,
      "first_name": "Борис",
      "second_name": "Закарая",
      "user_id": 54,
      "del_reason": "finish",
      "begin_time": 1474106104,
      "end_time": 1474106383,
      "session_id": 3147,
      "time": 19977,
      "questions": [
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 14552,
          "question_id": 434,
          "position": 1,
          "group_id": 635,
          "answers": [
            {
              "answer_id": None,
              "text": "1110000",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 5425,
          "question_id": 435,
          "position": 2,
          "group_id": 636,
          "answers": [
            {
              "answer_id": None,
              "text": "1000",
              "correct_answer": False
            }
          ]
        }
      ]
    },
    {
      "workingUser_id": 5101,
      "first_name": "Даниил",
      "second_name": None,
      "user_id": None,
      "del_reason": "finish",
      "begin_time": 1474105999,
      "end_time": 1474106403,
      "session_id": 3106,
      "time": 18265,
      "questions": [
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 4518,
          "question_id": 432,
          "position": 1,
          "group_id": 635,
          "answers": [
            {
              "answer_id": None,
              "text": "1100100",
              "correct_answer": True
            }
          ]
        },
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 6166,
          "question_id": 437,
          "position": 2,
          "group_id": 636,
          "answers": [
            {
              "answer_id": None,
              "text": "-110",
              "correct_answer": True
            }
          ]
        },
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 4184,
          "question_id": 446,
          "position": 3,
          "group_id": 639,
          "answers": [
            {
              "answer_id": None,
              "text": "1011",
              "correct_answer": True
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 1237,
          "question_id": 450,
          "position": 4,
          "group_id": 640,
          "answers": [
            {
              "answer_id": None,
              "text": "336",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 1333,
          "question_id": 456,
          "position": 5,
          "group_id": 642,
          "answers": [
            {
              "answer_id": None,
              "text": "D3",
              "correct_answer": True
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 827,
          "question_id": 457,
          "position": 6,
          "group_id": 643,
          "answers": [
            {
              "answer_id": None,
              "text": "D0",
              "correct_answer": False
            }
          ]
        }
      ]
    },
    {
      "workingUser_id": 5146,
      "first_name": "Машенька",
      "second_name": None,
      "user_id": None,
      "del_reason": "finish",
      "begin_time": 1474106133,
      "end_time": 1474106435,
      "session_id": 3151,
      "time": 19721,
      "questions": [
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 3098,
          "question_id": 433,
          "position": 1,
          "group_id": 635,
          "answers": [
            {
              "answer_id": None,
              "text": "1100000",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 7198,
          "question_id": 436,
          "position": 2,
          "group_id": 636,
          "answers": [
            {
              "answer_id": None,
              "text": "11000",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 3703,
          "question_id": 447,
          "position": 3,
          "group_id": 639,
          "answers": [
            {
              "answer_id": None,
              "text": "1050",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 5722,
          "question_id": 448,
          "position": 4,
          "group_id": 640,
          "answers": [
            {
              "answer_id": None,
              "text": "-123",
              "correct_answer": False
            }
          ]
        }
      ]
    },
    {
      "workingUser_id": 5118,
      "first_name": "Траляля",
      "second_name": None,
      "user_id": None,
      "del_reason": "finish",
      "begin_time": 1474106034,
      "end_time": 1474106463,
      "session_id": 3123,
      "time": 18719,
      "questions": [
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 4516,
          "question_id": 432,
          "position": 1,
          "group_id": 635,
          "answers": [
            {
              "answer_id": None,
              "text": "1100100",
              "correct_answer": True
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 5321,
          "question_id": 436,
          "position": 2,
          "group_id": 636,
          "answers": [
            {
              "answer_id": None,
              "text": "110",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 1768,
          "question_id": 447,
          "position": 3,
          "group_id": 639,
          "answers": [
            {
              "answer_id": None,
              "text": "1040",
              "correct_answer": True
            }
          ]
        },
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 3467,
          "question_id": 449,
          "position": 4,
          "group_id": 640,
          "answers": [
            {
              "answer_id": None,
              "text": "-117",
              "correct_answer": True
            }
          ]
        },
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 1476,
          "question_id": 455,
          "position": 5,
          "group_id": 642,
          "answers": [
            {
              "answer_id": None,
              "text": "D2",
              "correct_answer": True
            }
          ]
        },
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 2171,
          "question_id": 459,
          "position": 6,
          "group_id": 643,
          "answers": [
            {
              "answer_id": None,
              "text": "-3D",
              "correct_answer": True
            }
          ]
        }
      ]
    },
    {
      "workingUser_id": 5147,
      "first_name": "Андрей",
      "second_name": "Антипов",
      "user_id": 52,
      "del_reason": "left",
      "begin_time": None,
      "end_time": 1474106508,
      "session_id": 3152,
      "time": 0,
      "questions": []
    },
    {
      "workingUser_id": 5120,
      "first_name": "Мария",
      "second_name": None,
      "user_id": None,
      "del_reason": "finish",
      "begin_time": 1474106029,
      "end_time": 1474106681,
      "session_id": 3124,
      "time": 955,
      "questions": [
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 955,
          "question_id": 432,
          "position": 1,
          "group_id": 635,
          "answers": [
            {
              "answer_id": None,
              "text": "2",
              "correct_answer": False
            }
          ]
        }
      ]
    },
    {
      "workingUser_id": 5112,
      "first_name": "Helloworld",
      "second_name": None,
      "user_id": None,
      "del_reason": "finish",
      "begin_time": 1474106145,
      "end_time": 1474106146,
      "session_id": 3117,
      "time": 12707,
      "questions": []
    },
    {
      "workingUser_id": 5100,
      "first_name": "Вадим",
      "second_name": None,
      "user_id": None,
      "del_reason": "finish",
      "begin_time": 1474105992,
      "end_time": 1474106219,
      "session_id": 3105,
      "time": 21328,
      "questions": [
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 4658,
          "question_id": 432,
          "position": 1,
          "group_id": 635,
          "answers": [
            {
              "answer_id": None,
              "text": "1100100",
              "correct_answer": True
            }
          ]
        },
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 6384,
          "question_id": 436,
          "position": 2,
          "group_id": 636,
          "answers": [
            {
              "answer_id": None,
              "text": "-110",
              "correct_answer": True
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 2834,
          "question_id": 446,
          "position": 3,
          "group_id": 639,
          "answers": [
            {
              "answer_id": None,
              "text": "1122",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 2336,
          "question_id": 448,
          "position": 4,
          "group_id": 640,
          "answers": [
            {
              "answer_id": None,
              "text": "-123",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 2336,
          "question_id": 456,
          "position": 5,
          "group_id": 642,
          "answers": [
            {
              "answer_id": None,
              "text": "D3",
              "correct_answer": True
            }
          ]
        },
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 2780,
          "question_id": 459,
          "position": 6,
          "group_id": 643,
          "answers": [
            {
              "answer_id": None,
              "text": "-3D",
              "correct_answer": True
            }
          ]
        }
      ]
    },
    {
      "workingUser_id": 5098,
      "first_name": "Polina Kim",
      "second_name": None,
      "user_id": None,
      "del_reason": "finish",
      "begin_time": 1474105981,
      "end_time": 1474106230,
      "session_id": 3103,
      "time": 22757,
      "questions": [
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 3379,
          "question_id": 434,
          "position": 1,
          "group_id": 635,
          "answers": [
            {
              "answer_id": None,
              "text": "1110000",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 6269,
          "question_id": 437,
          "position": 2,
          "group_id": 636,
          "answers": [
            {
              "answer_id": None,
              "text": "-110",
              "correct_answer": True
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 3230,
          "question_id": 445,
          "position": 3,
          "group_id": 639,
          "answers": [
            {
              "answer_id": None,
              "text": "1021",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 1840,
          "question_id": 450,
          "position": 4,
          "group_id": 640,
          "answers": [
            {
              "answer_id": None,
              "text": "306",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 5156,
          "question_id": 455,
          "position": 5,
          "group_id": 642,
          "answers": [
            {
              "answer_id": None,
              "text": "D1",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 2883,
          "question_id": 458,
          "position": 6,
          "group_id": 643,
          "answers": [
            {
              "answer_id": None,
              "text": "6A",
              "correct_answer": False
            }
          ]
        }
      ]
    },
    {
      "workingUser_id": 5099,
      "first_name": "Валерий",
      "second_name": None,
      "user_id": None,
      "del_reason": "finish",
      "begin_time": 1474106000,
      "end_time": 1474106254,
      "session_id": 3104,
      "time": 24000,
      "questions": [
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 3447,
          "question_id": 432,
          "position": 1,
          "group_id": 635,
          "answers": [
            {
              "answer_id": None,
              "text": "1100100",
              "correct_answer": True
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 6539,
          "question_id": 437,
          "position": 2,
          "group_id": 636,
          "answers": [
            {
              "answer_id": None,
              "text": "11010",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 3284,
          "question_id": 446,
          "position": 3,
          "group_id": 639,
          "answers": [
            {
              "answer_id": None,
              "text": "1012",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 3104,
          "question_id": 450,
          "position": 4,
          "group_id": 640,
          "answers": [
            {
              "answer_id": None,
              "text": "306",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 4590,
          "question_id": 452,
          "position": 5,
          "group_id": 642,
          "answers": [
            {
              "answer_id": None,
              "text": "1CA",
              "correct_answer": False
            }
          ]
        }
      ]
    },
    {
      "workingUser_id": 5115,
      "first_name": "Серафим",
      "second_name": None,
      "user_id": None,
      "del_reason": "finish",
      "begin_time": 1474106012,
      "end_time": 1474106256,
      "session_id": 3120,
      "time": 18209,
      "questions": [
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 7312,
          "question_id": 434,
          "position": 1,
          "group_id": 635,
          "answers": [
            {
              "answer_id": None,
              "text": "1110000",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 10897,
          "question_id": 435,
          "position": 2,
          "group_id": 636,
          "answers": [
            {
              "answer_id": None,
              "text": "000000",
              "correct_answer": False
            }
          ]
        }
      ]
    },
    {
      "workingUser_id": 5119,
      "first_name": "Blablabla",
      "second_name": None,
      "user_id": None,
      "del_reason": "finish",
      "begin_time": 1474106133,
      "end_time": 1474106275,
      "session_id": 3125,
      "time": 15693,
      "questions": [
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 8300,
          "question_id": 432,
          "position": 1,
          "group_id": 635,
          "answers": [
            {
              "answer_id": None,
              "text": "1100100",
              "correct_answer": True
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 5652,
          "question_id": 437,
          "position": 2,
          "group_id": 636,
          "answers": [
            {
              "answer_id": None,
              "text": "110",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 1426,
          "question_id": 446,
          "position": 3,
          "group_id": 639,
          "answers": [
            {
              "answer_id": None,
              "text": "1011",
              "correct_answer": True
            }
          ]
        }
      ]
    },
    {
      "workingUser_id": 5122,
      "first_name": "Сергей",
      "second_name": None,
      "user_id": None,
      "del_reason": "finish",
      "begin_time": 1474106035,
      "end_time": 1474106281,
      "session_id": 3127,
      "time": 19761,
      "questions": [
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 4824,
          "question_id": 432,
          "position": 1,
          "group_id": 635,
          "answers": [
            {
              "answer_id": None,
              "text": "1010100",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 14937,
          "question_id": 437,
          "position": 2,
          "group_id": 636,
          "answers": [
            {
              "answer_id": None,
              "text": "-110",
              "correct_answer": True
            }
          ]
        }
      ]
    },
    {
      "workingUser_id": 5114,
      "first_name": "Дмитрий Бурлаев",
      "second_name": None,
      "user_id": None,
      "del_reason": "finish",
      "begin_time": 1474106003,
      "end_time": 1474106294,
      "session_id": 3119,
      "time": 12181,
      "questions": [
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 3257,
          "question_id": 434,
          "position": 1,
          "group_id": 635,
          "answers": [
            {
              "answer_id": None,
              "text": "1110000",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 5966,
          "question_id": 437,
          "position": 2,
          "group_id": 636,
          "answers": [
            {
              "answer_id": None,
              "text": "0110",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 2958,
          "question_id": 446,
          "position": 3,
          "group_id": 639,
          "answers": [
            {
              "answer_id": None,
              "text": "1020",
              "correct_answer": False
            }
          ]
        }
      ]
    },
    {
      "workingUser_id": 5113,
      "first_name": "Илья",
      "second_name": None,
      "user_id": None,
      "del_reason": "finish",
      "begin_time": 1474106022,
      "end_time": 1474106299,
      "session_id": 3118,
      "time": 24000,
      "questions": [
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 5959,
          "question_id": 434,
          "position": 1,
          "group_id": 635,
          "answers": [
            {
              "answer_id": None,
              "text": "1110000",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 6062,
          "question_id": 436,
          "position": 2,
          "group_id": 636,
          "answers": [
            {
              "answer_id": None,
              "text": "-10110",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 2779,
          "question_id": 447,
          "position": 3,
          "group_id": 639,
          "answers": [
            {
              "answer_id": None,
              "text": "1040",
              "correct_answer": True
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 3024,
          "question_id": 450,
          "position": 4,
          "group_id": 640,
          "answers": [
            {
              "answer_id": None,
              "text": "306",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 4487,
          "question_id": 455,
          "position": 5,
          "group_id": 642,
          "answers": [
            {
              "answer_id": None,
              "text": "d2",
              "correct_answer": True
            }
          ]
        }
      ]
    },
    {
      "workingUser_id": 5117,
      "first_name": "Иван",
      "second_name": None,
      "user_id": None,
      "del_reason": "finish",
      "begin_time": 1474106016,
      "end_time": 1474106301,
      "session_id": 3122,
      "time": 23404,
      "questions": [
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 6346,
          "question_id": 432,
          "position": 1,
          "group_id": 635,
          "answers": [
            {
              "answer_id": None,
              "text": "1100100",
              "correct_answer": True
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 7740,
          "question_id": 437,
          "position": 2,
          "group_id": 636,
          "answers": [
            {
              "answer_id": None,
              "text": "10010",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 2289,
          "question_id": 447,
          "position": 3,
          "group_id": 639,
          "answers": [
            {
              "answer_id": None,
              "text": "1050",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 7029,
          "question_id": 449,
          "position": 4,
          "group_id": 640,
          "answers": [
            {
              "answer_id": None,
              "text": "-117",
              "correct_answer": True
            }
          ]
        }
      ]
    },
    {
      "workingUser_id": 5132,
      "first_name": "Сергей",
      "second_name": "Коваленко",
      "user_id": 53,
      "del_reason": "finish",
      "begin_time": 1474106051,
      "end_time": 1474106303,
      "session_id": 3137,
      "time": 18950,
      "questions": [
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 7808,
          "question_id": 433,
          "position": 1,
          "group_id": 635,
          "answers": [
            {
              "answer_id": None,
              "text": "1100000",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 11142,
          "question_id": 436,
          "position": 2,
          "group_id": 636,
          "answers": [
            {
              "answer_id": None,
              "text": "000110",
              "correct_answer": False
            }
          ]
        }
      ]
    },
    {
      "workingUser_id": 5111,
      "first_name": "Андрей",
      "second_name": "Антипов",
      "user_id": 52,
      "del_reason": "finish",
      "begin_time": 1474105999,
      "end_time": 1474106314,
      "session_id": 3116,
      "time": 24000,
      "questions": [
        {
          "correct_answers": 0,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 4314,
          "question_id": 433,
          "position": 1,
          "group_id": 635,
          "answers": [
            {
              "answer_id": None,
              "text": "1100000",
              "correct_answer": False
            }
          ]
        },
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 3048,
          "question_id": 435,
          "position": 2,
          "group_id": 636,
          "answers": [
            {
              "answer_id": None,
              "text": "110",
              "correct_answer": True
            }
          ]
        },
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 5108,
          "question_id": 446,
          "position": 3,
          "group_id": 639,
          "answers": [
            {
              "answer_id": None,
              "text": "1011",
              "correct_answer": True
            }
          ]
        },
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 5726,
          "question_id": 448,
          "position": 4,
          "group_id": 640,
          "answers": [
            {
              "answer_id": None,
              "text": "-134",
              "correct_answer": True
            }
          ]
        },
        {
          "correct_answers": 1,
          "type": 2,
          "all_correct_answers": 1,
          "answer_time": 5099,
          "question_id": 456,
          "position": 5,
          "group_id": 642,
          "answers": [
            {
              "answer_id": None,
              "text": "D3",
              "correct_answer": True
            }
          ]
        }
      ]
    }
  ],
  "test_content": {
    "questions": {
      "432": {
        "text": "<p>вычислите (систему счисления в ответе не указывайте)</p><p><span>110101<sub>b</sub> + 101111<sub>b</sub></span><br/></p>"
      },
      "433": {
        "text": "<p>вычислите (систему счисления в ответе не указывайте)</p><p><!--startfragment--><span style=\"color: rgb(85, 85, 85);float: none;background-color: rgb(255, 255, 255);\">101101<sub>b </sub> <span style=\"color: rgb(85, 85, 85);float: none;background-color: rgb(255, 255, 255);\">+ 110011<sub>b</sub></span><!--endfragment--><br/><br/></span><br/></p>"
      },
      "434": {
        "text": "<p style=\"color: rgb(85, 85, 85);background-color: rgb(255, 255, 255);\">вычислите (систему счисления в ответе не указывайте)</p><p style=\"color: rgb(85, 85, 85);background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(85, 85, 85);float: none;background-color: rgb(255, 255, 255);\">110101<sub>b<span class=\"apple-converted-space\"> </span></sub> <span style=\"color: rgb(85, 85, 85);float: none;background-color: rgb(255, 255, 255);\">+ 111011<sub>b</sub></span></span></p>"
      },
      "435": {
        "text": "<p>вычислите (систему счисления в ответе не указывайте)</p><p><span>110101<sub>b</sub> - 101111<sub>b</sub></span></p><p><br/></p><p></p>"
      },
      "436": {
        "text": "<p>вычислите (систему счисления в ответе не указывайте)</p><p><span style=\"color: rgb(85, 85, 85);float: none;background-color: rgb(255, 255, 255);\">101101<sub>b </sub> <span style=\"color: rgb(85, 85, 85);float: none;background-color: rgb(255, 255, 255);\">- 110011<sub>b</sub></span><!--endfragment--><br/><br/></span></p><p><br/></p><p><!--startfragment--></p>"
      },
      "437": {
        "text": "<p style=\"color: rgb(85, 85, 85);background-color: rgb(255, 255, 255);\">вычислите (систему счисления в ответе не указывайте)</p><p><span style=\"color: rgb(85, 85, 85);float: none;background-color: rgb(255, 255, 255);\">110101<sub>b<span class=\"apple-converted-space\"> </span></sub> <span style=\"color: rgb(85, 85, 85);float: none;background-color: rgb(255, 255, 255);\">- 111011<sub>b</sub></span></span></p><p style=\"color: rgb(85, 85, 85);background-color: rgb(255, 255, 255);\"></p><p><br/></p><p></p><p><br/></p><p></p>"
      },
      "445": {
        "text": "<p>вычислите (систему счисления в ответе не указывайте)</p><p><span>423<sub>o</sub> + 367<sub>o</sub></span></p><p></p><p></p><p></p><p></p><p><br/></p><p></p><p></p><p></p><p></p><p></p>"
      },
      "446": {
        "text": "<p>вычислите (систему счисления в ответе не указывайте)</p><p><span>314<sub>o</sub> + 475<sub>o</sub></span></p>"
      },
      "447": {
        "text": "<p>вычислите (систему счисления в ответе не указывайте)</p><p><span>563<sub>o</sub> + 255<sub>o</sub></span></p><p></p><p></p><p></p><p></p><p></p><p> </p><p></p><p></p><p></p><p></p><p></p><p></p>"
      },
      "448": {
        "text": "<p>вычислите (систему счисления в ответе не указывайте)</p><p><span>167<sub>o</sub> - 323<sub>o</sub></span></p><p></p><p></p><p></p><p></p><p><br/></p><p></p><p></p><p></p><p></p><p></p>"
      },
      "449": {
        "text": "<p>вычислите (систему счисления в ответе не указывайте)</p><p><span>375<sub>o</sub> - 514<sub>o</sub></span></p>"
      },
      "450": {
        "text": "<p>вычислите (систему счисления в ответе не указывайте)</p><p><span>563<sub>o</sub> - 255<sub>o</sub></span></p><p></p><p></p><p></p><p></p><p></p><p><br/></p><p></p><p></p><p></p><p></p><p></p><p></p>"
      },
      "452": {
        "text": "<p>вычислите (систему счисления в ответе не указывайте)</p><p><span>A3<sub>h</sub> + 87<sub>h</sub></span></p><p></p><p></p><p></p><p></p><p><br/></p><p></p><p></p><p></p><p></p><p></p>"
      },
      "455": {
        "text": "<p>вычислите (систему счисления в ответе не указывайте)</p><p><span>9B<sub>h</sub> + 37<sub>h</sub></span></p><p></p><p></p><p></p><p></p><p><br/></p><p></p><p></p><p></p><p></p><p></p>"
      },
      "456": {
        "text": "<p>вычислите (систему счисления в ответе не указывайте)</p><p><span>8A<sub>h</sub> + 49<sub>h</sub></span></p><p></p><p></p><p></p><p></p><p><br/></p><p></p><p></p><p></p><p></p><p></p>"
      },
      "457": {
        "text": "<p>вычислите (систему счисления в ответе не указывайте)</p><p>87<sub>h</sub> - A2<sub>h</sub></p><p></p><p></p><p></p><p></p><p><br/></p><p></p><p></p><p></p><p></p><p></p>"
      },
      "458": {
        "text": "<p>вычислите (систему счисления в ответе не указывайте)</p><p>3B<sub>h</sub> - 97<sub>h</sub></p><p></p><p></p><p></p><p></p><p><br/></p><p></p><p></p><p></p><p></p><p></p>"
      },
      "459": {
        "text": "<p>вычислите (систему счисления в ответе не указывайте)</p><p>4C<sub>h</sub> - 89<sub>h</sub></p><p></p><p></p><p></p><p></p><p><br/></p><p></p><p></p><p></p><p></p><p></p>"
      }
    },
    "answers": {
      "1055": {
        "text": "1100100"
      },
      "1056": {
        "text": "1100100"
      },
      "1057": {
        "text": "110000"
      },
      "1058": {
        "text": "110"
      },
      "1059": {
        "text": "-110"
      },
      "1060": {
        "text": "-110"
      },
      "1068": {
        "text": "1012"
      },
      "1069": {
        "text": "1011"
      },
      "1070": {
        "text": "1040"
      },
      "1071": {
        "text": "-134"
      },
      "1072": {
        "text": "-117"
      },
      "1073": {
        "text": "-306"
      },
      "1075": {
        "text": "12a"
      },
      "1078": {
        "text": "12A"
      },
      "1079": {
        "text": "D2"
      },
      "1080": {
        "text": "d2"
      },
      "1081": {
        "text": "D3"
      },
      "1082": {
        "text": "d3"
      },
      "1083": {
        "text": "-1b"
      },
      "1084": {
        "text": "-1B"
      },
      "1085": {
        "text": "-5e"
      },
      "1086": {
        "text": "-5E"
      },
      "1087": {
        "text": "-3D"
      },
      "1088": {
        "text": "-3d"
      }
    }
  }
}
    s = StreamReport(stream, users_data, questions_data)
    print('{} ч {} мин {} сек'.format(*s._get_time_from_ms(18212*10)))