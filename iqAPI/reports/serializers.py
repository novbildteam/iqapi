import collections
from rest_framework import serializers

from reports.models import Lessons
from reports.models import Streams, Sessions, Tests, Results, Answers, Questions, Groups


# need to convert datetime to int
class IntDateTimeField(serializers.ReadOnlyField):
    def to_representation(self, value):
        return int(value.timestamp())


class LessonsSerializer(serializers.ModelSerializer):
    """
    lesson_id     integer
    title    string
    description    string
    begin_time    integer    lesson begin time (unix time)
    end_time    integer   lesson end time (unix    time)
    streams    number    count of streams
    users    number    count of users
    """

    begin_time = IntDateTimeField(source='start_time')
    end_time = IntDateTimeField()
    title = serializers.CharField(source='subject.title')
    streams = serializers.IntegerField(source='streams_set.count')
    users = serializers.SerializerMethodField()


    class Meta:
        model = Lessons
        fields = ('lesson_id', 'title', 'description', 'begin_time', 'end_time', 'streams', 'users', 'subject_id')


    def get_users(self, obj):

        streams_sessions = [stream.sessions_set.all() for stream in obj.streams_set.all()]
        sessions = []
        for s in streams_sessions:
            sessions.extend(s)
        users = {session.working_user_id for session in sessions}
        return len(users)


class SessionUserSerializer(serializers.Serializer):

    """
    workingUser_id     идентификатор    студента
    first_name    имя
    second_name    фамилия
    rate    процент    правильных    ответов    на    тесты(не    опросы) (надо ли?)
    """

    workingUser_id = serializers.PrimaryKeyRelatedField(source='working_user.working_user_id', read_only=True)
    first_name = serializers.SerializerMethodField()
    second_name = serializers.SerializerMethodField()
    user_id = serializers.IntegerField(source='working_user.user.user_id')

    def get_first_name(self, obj):
        user = obj.working_user.user
        if user:
            return list(user.userprofile_set.all())[0].profile_first_name
        return obj.working_user.first_name

    def get_second_name(self, obj):
        user = obj.working_user.user
        if user:
            return list(user.userprofile_set.all())[0].profile_second_name
        return obj.working_user.second_name


class SessionUserExtSerializer(SessionUserSerializer):
    """
    del_reason string
    time integer
    begin time integer
    end_time integer
    """

    del_reason = serializers.CharField()
    begin_time = IntDateTimeField()
    end_time = IntDateTimeField()


class OverrideSerializer(serializers.Serializer):
    random = serializers.BooleanField()
    timer = serializers.IntegerField()


class TestSerializer(serializers.ModelSerializer):
    """
    owner_id	integer owner of test (fk for users table)
    name	str
    type	integer (0 - test, 1 - poll)
    questions	integer - count of questions
    timer   integer - test timer (if exists)
    random  boolean - shuffle questions
    description string
    test_id integer
    branch_id	integer
    """

    owner_id = serializers.PrimaryKeyRelatedField(source='branch.owner_id', read_only=True)
    branch_id = serializers.PrimaryKeyRelatedField(source='branch.branch_id', read_only=True)
    name = serializers.CharField(source='branch.branch_name')
    type = serializers.IntegerField(source='branch.branch_type')
    questions = serializers.SerializerMethodField(source='positions')
    timer = serializers.IntegerField(source='test_c.test_timer')
    random = serializers.IntegerField(source='test_c.test_random')
    description = serializers.CharField(source='test_c.test_description')
    test_id = serializers.PrimaryKeyRelatedField(read_only=True)

    def get_questions(self, obj):
        return len(obj.positions)

    class Meta:
        model = Tests
        fields = read_only_fields = ('owner_id', 'branch_id', 'name', 'type', 'questions',
                                     'timer', 'random', 'description', 'test_id')



class LessonStreamsSerializer(serializers.ModelSerializer):
    # stream_id - ~    number     идентификатор    стрима
    # test - ~    testObject    объект    теста
    # override - ~    overrideObject    объект    перегрузки
    # add_time - ~    number    время    начала стрима
    # del_time - ~    number    время    конца    стрима
    # del_reason - ~    string    причина    завершения    стрима
    # pace_mode - ~    number    режим    прохождения

    add_time = IntDateTimeField()
    del_time = IntDateTimeField()
    test = serializers.SerializerMethodField(source='test', method_name='get_test_object')
    override = serializers.SerializerMethodField(source='override')
    lesson_id = serializers.PrimaryKeyRelatedField(source='lesson', read_only=True)

    class Meta:
        model = Streams
        depth = 1
        fields = ('stream_id', 'test', 'override', 'add_time', 'del_time', 'del_reason', 'pace_mode', 'lesson_id')

    def get_override(self, obj):
        serializer = OverrideSerializer(obj.override)
        return serializer.data

    def get_test_object(self,obj):
        serializer = TestSerializer(obj.test)
        return serializer.data


class UserAnswerSerializer(serializers.ModelSerializer):

    result_id = serializers.IntegerField()
    answer_id = serializers.PrimaryKeyRelatedField(source='answer', read_only=True)
    text = serializers.CharField(source='open_answer_text')
    correct_answer = serializers.BooleanField(source='answer.answer_v.answer_c.correct_answer')

    class Meta:
        model = Answers
        fields = read_only_fields = ('result_id', 'answer_id', 'text', 'correct_answer')


class RawResultSerializer(serializers.ModelSerializer):
    result_id = serializers.PrimaryKeyRelatedField(read_only=True)
    question_id = serializers.PrimaryKeyRelatedField(source='question', read_only=True)
    group_id = serializers.SerializerMethodField()
    answer_id = serializers.PrimaryKeyRelatedField(source='answer', read_only=True)
    session_id = serializers.PrimaryKeyRelatedField(source='session', read_only=True)
    type = serializers.IntegerField(source='question.question_v.question_c.question_type')
    text = serializers.CharField(source='open_answer_text')
    position = serializers.SerializerMethodField()
    answer_time = serializers.SerializerMethodField()

    class Meta:
        model = Results
        fields = read_only_fields = ('question_id', 'group_id', 'session_id', 'answer_id',
                                     'type', 'text', 'position', 'answer_time',)

    def get_group_id(self, obj):
        group_v_id = obj.question.group_v.group_v_id
        groups = list(obj.test.groups_set.all())
        group_id = [g.group_id for g in groups if g.group_v_id == group_v_id][0]
        return group_id

    def get_answer_time(self, obj):
        res_list = list(obj.session.resultstime_set.all())
        for res in res_list:
            if res.question == obj.question:
                return res.time
        return None  # it's impossible

    def get_position(self, obj):

        #  Optimization idea:
        #  map group_id to group_v_id and then search in obj.test.positions

        group_v_id = obj.question.group_v.group_v_id
        groups = list(obj.test.groups_set.all())
        groups_map = {g.group_id: g.group_v_id for g in groups}
        mapped_positions = []
        for g in obj.test.positions:
            mapped_positions.append(groups_map[g])
        position = mapped_positions.index(group_v_id) + 1
        return position


class RawResultRateSerializer(RawResultSerializer):

    correct_answer = serializers.SerializerMethodField()
    all_correct_answers = serializers.SerializerMethodField(required=False)

    class Meta:
        model = RawResultSerializer.Meta.model
        fields = read_only_fields = RawResultSerializer.Meta.fields + ('correct_answer', 'all_correct_answers')

    def get_correct_answer(self, obj):
        return self.is_correct(obj)

    def get_all_correct_answers(self, obj):
        question_type = obj.question.question_v.question_c.question_type

        if question_type == 1:
            count = 0
            for answer in list(obj.question.question_v.answers_set.all()):
                if answer.answer_v.answer_c.correct_answer is True:
                    count += 1
            all_correct_answers = count
        elif question_type == 0 or question_type == 2:
            all_correct_answers = 1
        else:
            # future types
            all_correct_answers = 0
        return all_correct_answers

    def is_correct(self, obj):
        if obj.answer:
            return obj.answer.answer_v.answer_c.correct_answer is True
        else:
            open_answers = [answer.answer_v.answer_c.answer_text for answer in obj.question.question_v.answers_set.all()]
            return obj.open_answer_text in open_answers


class UserResultDefaultSerializer(RawResultSerializer):

    class Meta:
        model = RawResultSerializer.Meta.model
        fields = read_only_fields = ('question_id', 'group_id', 'answer_id', 'type', 'position', 'text', 'answer_time')

    # method that helps correctly render set of results
    @staticmethod
    def results_by_questions(results):

        new_repr = collections.OrderedDict()

        for question in results:
            question_id = question['question_id']
            if question_id not in new_repr:
                new_repr[question_id] = collections.OrderedDict({
                    'question_id': question_id,
                    'group_id': question['group_id'],
                    'type': question['type'],
                    'position': question['position'],
                    'answer_time': question['answer_time'],
                })
                new_repr[question_id]['answers'] = []
            # put answers without fields of questions
            new_repr[question_id]['answers'].append({k: v for k, v in question.items() if k not in new_repr[question_id]})
        new_repr = [v for k, v in new_repr.items()]
        return new_repr


class UserResultRateSerializer(RawResultRateSerializer):

    class Meta:
        model = RawResultRateSerializer.Meta.model
        fields = read_only_fields = ('question_id', 'group_id', 'answer_id', 'type', 'text',
                                     'position', 'answer_time', 'correct_answer', 'all_correct_answers')

    @staticmethod
    def results_by_questions(results):

        new_repr = collections.OrderedDict()

        for question in results:
            question_id = question['question_id']
            if question_id not in new_repr:
                new_repr[question_id] = collections.OrderedDict({
                    'question_id': question_id,
                    'group_id': question['group_id'],
                    'type': question['type'],
                    'position': question['position'],
                    'answer_time': question['answer_time'],
                    'all_correct_answers': question['all_correct_answers'],
                    'correct_answers': 0,
                })
                new_repr[question_id]['answers'] = []
            # sum of correct answers
            if question['correct_answer'] is True:
                new_repr[question_id]['correct_answers'] += 1
            # put answers without fields of questions
            new_repr[question_id]['answers'].append({k: v for k, v in question.items() if k not in new_repr[question_id]})
        new_repr = [v for k, v in new_repr.items()]
        return new_repr


class QuestionResultDefaultSerializer(RawResultSerializer):
    @staticmethod
    def results_by_questions(results, full_test=None):

        groups = collections.OrderedDict()
        questions = collections.OrderedDict()
        answers = {}

        # if full_test exists then merging it with results
        if full_test is not None:
            for group in full_test['groups']:
                group_id = group['group_id']
                groups[group_id] = {
                    'group_id': group_id,
                    'position': group['position'],
                    'count': 0,
                    'questions': [],
                }
                for question in group['questions']:
                    question_id = question['question_id']
                    questions[question_id] = {
                        'question_id': question_id,
                        'type': question['type'],
                        'count': 0,
                        'answers': [],
                        'sessions': [],
                    }
                    groups[group_id]['questions'].append(questions[question_id])
                    for answer in question['answers']:
                        answer_id = answer['answer_id']
                        if question['type'] == 0 or question['type'] == 1:
                            answers[answer_id] = {
                                'answer_id': answer_id,
                                'count': 0,
                            }
                        if question['type'] == 2:
                            answer_text = answer['text']
                            answers[answer_id] = {
                                'answer_id': answer_id,
                                'text': answer_text,
                                'count': 0,
                            }
                        questions[question_id]['answers'].append(answers[answer_id])

        for result in results:
            group_id = result['group_id']
            question_id = result['question_id']
            session_id = result['session_id']
            if group_id not in groups:
                groups[group_id] = {
                    'group_id': group_id,
                    'position': result['position'],
                    'count': 0,
                    'questions': [],
                }
            if question_id not in questions:
                questions[question_id] = {
                    'question_id': question_id,
                    'type': result['type'],
                    'count': 0,
                    'answers': [],
                    'sessions': [],
                }
                groups[group_id]['questions'].append(questions[question_id])
            if not session_id in questions[question_id]['sessions']:
                questions[question_id]['sessions'].append(session_id)
                questions[question_id]['count'] += 1
                groups[group_id]['count'] += 1
            # close question
            if result['type'] == 0 or result['type'] == 1:
                answer_id = result['answer_id']
                if not answer_id in answers:
                    answers[answer_id] = {
                        'answer_id': answer_id,
                        'count': 0,
                    }
                    questions[question_id]['answers'].append(answers[answer_id])
                answers[answer_id]['count'] += 1
            # open question
            if result['type'] == 2:
                answer_text = result['text']
                answer_id = None
                if 'answer_id' not in result or result['answer_id'] is None:
                    # this answer not in list of correct answers
                    # make answer_id
                    key_answer_id = str(question_id) + '_' + answer_text
                else:
                    # exists answer
                    key_answer_id = answer_id = result['answer_id']
                if not key_answer_id in answers:
                    answers[key_answer_id] = {
                        'text': answer_text,
                        'count': 0,
                    }
                    # if answer has real answer_id
                    if answer_id is not None:
                        answers[key_answer_id]['answer_id'] = answer_id
                    questions[question_id]['answers'].append(answers[key_answer_id])
                answers[key_answer_id]['count'] += 1
        # delete sessions info
        for k, v in questions.items():
            v.pop('sessions')
        return [v for k, v in groups.items()]


class QuestionResultRateSerializer(RawResultRateSerializer):
    @staticmethod
    def results_by_questions(results, full_test=None):

        sessions = {}
        groups = collections.OrderedDict()
        questions = collections.OrderedDict()
        answers = {}

        # if full_test exists then merging it with results
        if full_test is not None:
            for group in full_test['groups']:
                group_id = group['group_id']
                groups[group_id] = {
                    'group_id': group_id,
                    'position': group['position'],
                    'count': 0,
                    'questions': [],
                    'correct_answers': 0,
                    'correct_rate': 0,
                }
                for question in group['questions']:
                    question_id = question['question_id']
                    questions[question_id] = {
                        'question_id': question_id,
                        'type': question['type'],
                        'count': 0,
                        'correct_answers': 0,
                        'correct_rate': 0,
                        'all_correct_answers': 0, # sum of correct answers, see next
                        'answers': [],
                        'sessions': [],

                    }
                    groups[group_id]['questions'].append(questions[question_id])
                    for answer in question['answers']:
                        answer_id = answer['answer_id']
                        if question['type'] == 0 or question['type'] == 1:
                            answers[answer_id] = {
                                'answer_id': answer_id,
                                'correct': answer['correct_answer'],
                                'count': 0,
                            }
                        if question['type'] == 2:
                            answer_text = answer['text']
                            answers[answer_id] = {
                                'answer_id': answer_id,
                                'correct': answer['correct_answer'],
                                'text': answer_text,
                                'count': 0,
                            }
                        if answers[answer_id]['correct'] is True:
                            questions[question_id]['all_correct_answers'] += 1
                        questions[question_id]['answers'].append(answers[answer_id])

        for result in results:
            group_id = result['group_id']
            question_id = result['question_id']
            session_id = result['session_id']
            if session_id not in sessions:
                sessions[session_id] = {
                    'questions': {},
                }
            if group_id not in groups:
                groups[group_id] = collections.OrderedDict({
                    'group_id': group_id,
                    'position': result['position'],
                    'count': 0,
                    'correct_answers': 0,
                    'correct_rate': 0,
                    'questions': [],
                })
            if question_id not in questions:
                questions[question_id] = collections.OrderedDict({
                    'question_id': question_id,
                    'type': result['type'],
                    'all_correct_answers': result['all_correct_answers'],
                    'correct_answers': 0,
                    'correct_rate': 0,
                    'count': 0,
                    'answers': [],
                    'sessions': [],
                })
                groups[group_id]['questions'].append(questions[question_id])
            if question_id not in sessions[session_id]['questions']:
                session_question = {
                    'question_id': question_id,
                    'all_correct_answers': result['all_correct_answers'],
                    'type': result['type'],
                    'answers': {}
                }
                sessions[session_id]['questions'][question_id] = session_question
            if not session_id in questions[question_id]['sessions']:
                questions[question_id]['sessions'].append(session_id)
                questions[question_id]['count'] += 1
                groups[group_id]['count'] += 1
            # close question
            if result['type'] == 0 or result['type'] == 1:
                answer_id = result['answer_id']
                if not answer_id in answers:
                    answers[answer_id] = {
                        'answer_id': answer_id,
                        'correct': result['correct_answer'],
                        'count': 0,
                    }
                    questions[question_id]['answers'].append(answers[answer_id])
                answers[answer_id]['count'] += 1
                session_question['answers'][answer_id] = answers[answer_id]
            # open question
            if result['type'] == 2:
                answer_text = result['text']
                answer_id = None
                if 'answer_id' not in result or result['answer_id'] is None:
                    # this answer not in list of correct answers
                    # make answer_id
                    key_answer_id = str(question_id) + '_' + answer_text
                else:
                    # exists answer
                    key_answer_id = answer_id = result['answer_id']
                if not key_answer_id in answers:
                    answers[key_answer_id] = {
                        'text': answer_text,
                        'correct': result['correct_answer'],
                        'count': 0,
                    }
                    if answer_id is not None:
                        answers[key_answer_id]['answer_id'] = answer_id
                    questions[question_id]['answers'].append(answers[key_answer_id])
                answers[key_answer_id]['count'] += 1
                session_question['answers'][key_answer_id] = answers[key_answer_id]
        # calculate correct_answers and correct_rate for groups and questions
        for session_id, session in sessions.items():
            for question_id, s_question in session['questions'].items():
                correct_count = sum([s_answer['correct'] for answer_id, s_answer in s_question['answers'].items()])
                if correct_count == questions[question_id]['all_correct_answers'] and correct_count == len(s_question['answers']):
                    questions[question_id]['correct_answers'] += 1
        for group_id, group in groups.items():
            correct_answers = 0
            for question in group['questions']:
                if question['count'] > 0:
                    question['correct_rate'] = question['correct_answers']/question['count']
                else:
                    question['correct_rate'] = 0
                correct_answers += question['correct_answers']
            group['correct_answers'] = correct_answers
            if group['count'] > 0:
                group['correct_rate'] = correct_answers/group['count']
            else:
                group['correct_rate'] = 0

        # delete sessions info
        for k, v in questions.items():
            v.pop('sessions')
        return [v for k, v in groups.items()]


# user results serializer
class SessionUserResultsSerializer(SessionUserExtSerializer):

    """
     workingUser_id (r)   integer
     first_name (o)       string
     second_name (o)      string
     del_reason (r)       string
     time (r)             integer
     begin_time (r)       integer
     end_time  (r)        integer
     questions:  (see UserResultDefaultSerializer)
         question_id (r)  integer
         group_id (r)     integer
         text (o)         string
         position (r)     integer
         answer_time (r)  integer
         type (r)         integer
         answer_id (r)    integer
         text (o)         string
    """

    session_id = serializers.PrimaryKeyRelatedField(read_only=True)
    time = serializers.SerializerMethodField()  # time in santiseconds
    questions = serializers.SerializerMethodField(method_name='get_results')


    def get_results(self, obj):
        results_queryset = obj.results_set
        test_type = obj.stream.test.branch.branch_type
        serializer_class = UserResultDefaultSerializer
        if test_type == 0:
            serializer_class = UserResultRateSerializer
        result = serializer_class(results_queryset, many=True).data or []

        return serializer_class.results_by_questions(result)

    def get_time(self, obj):
        results = list(obj.resultstime_set.all())
        questions = {}
        # sum answer_time of results
        time_sum = 0
        for result in results:
            question_id = result.question.question_id
            # exclude repeating results (with many answers)
            if question_id in questions:
                continue
            questions[question_id] = True
            time_sum += result.time
        return time_sum

    @staticmethod
    def setup_eager_loading(queryset):
        queryset = queryset\
            .select_related('working_user__user', 'stream__test__branch', 'stream__test__test_c',)\
            .prefetch_related('working_user__user__userprofile_set',
                              'results_set__question__question_v__question_c',
                              'results_set__question__question_v__answers_set',
                              'results_set__question__question_v__answers_set__answer_v__answer_c',
                              'results_set__question__group_v',
                              'results_set__test',
                              'results_set__test__groups_set',
                              'results_set__answer__answer_v__answer_c',
                              'resultstime_set',
                              'resultstime_set__question',)
        return queryset


# Mixin for getting test content
class TestContentMixin():

    # test content representation
    _test_content = None
    # test content representation as hash table questions and answers
    _test_hash_table = None
    # instance of serializing object
    instance = None
    # test object link in dotted notation (string)
    test_object_link = None

    @property
    def test_content(self):
        if not self._test_content:
            if not self.instance:
                raise Exception('no instance')
            if not self.test_object_link:
                raise Exception('link to test object not defined')
            test_object = self.instance
            for key in self.test_object_link.split('.'):
                try:
                    test_object = getattr(test_object, key)
                except KeyError:
                    raise AttributeError('attribute "{}" not found in "test_object_link": {}'.format(key, self.test_object_link))
                except AttributeError:
                    raise Exception('there is no attributes in "test_object_link"')
            self._test_content = TestContentSerializer(test_object).data
        return self._test_content

    @property
    def test_hash_table(self):
        if not self._test_hash_table:
            test_content = self.test_content
            self._test_hash_table = TestContentSerializer.content_as_hashtable(test_content)
        return self._test_hash_table


# results of stream by questions
class StreamQuestionsResultsSerializer(LessonStreamsSerializer, TestContentMixin):

    groups = serializers.SerializerMethodField(method_name='get_results')
    test_content = serializers.SerializerMethodField()

    # TestContentMixin
    test_object_link = 'test'

    class Meta:
        model = LessonStreamsSerializer.Meta.model
        fields = read_only_fields = ('stream_id', 'groups', 'test_content')

    def get_results(self, obj):
        test_type = obj.test.branch.branch_type
        test_content = self.test_content

        results_queryset = Results.objects.filter(session__in=obj.sessions_set.all())
        results_queryset = results_queryset\
            .select_related('session__working_user__user',
                            'session__stream__test__branch',
                            'session__stream__test__test_c',
                            )\
            .prefetch_related('question__question_v__question_c',
                              'question__question_v__answers_set',
                              'question__question_v__answers_set__answer_v__answer_c',
                              'question__group_v',
                              'test',
                              'test__groups_set',
                              'answer__answer_v__answer_c',
                              'session__resultstime_set',
                              'session__resultstime_set__question',
                              'session__working_user__user__userprofile_set',)
        serializer_class = QuestionResultDefaultSerializer
        if test_type == 0:
           serializer_class = QuestionResultRateSerializer
        results = serializer_class(results_queryset, many=True).data or []
        questions_results = serializer_class.results_by_questions(results, test_content)
        return questions_results


    def get_test_content(self, obj):
        return self.test_hash_table


# results of stream by users
class StreamUsersResultsSerializer(LessonStreamsSerializer, TestContentMixin):

    users = serializers.SerializerMethodField()
    test_content = serializers.SerializerMethodField()

    # TestContentMixin
    test_object_link = 'test'

    class Meta:
        model = LessonStreamsSerializer.Meta.model
        fields = ('stream_id', 'users', 'test_content')

    def get_users(self, obj):
        results_serializer_class = SessionUserResultsSerializer
        sessions_queryset = obj.sessions_set
        sessions_queryset = results_serializer_class.setup_eager_loading(sessions_queryset)
        return results_serializer_class(sessions_queryset, many=True).data

    def get_test_content(self, obj):
        return self.test_hash_table


# test answers serializer
class TestAnswerSerializer(serializers.ModelSerializer):

    answer_id = serializers.PrimaryKeyRelatedField(read_only=True)
    text = serializers.CharField(source='answer_v.answer_c.answer_text')
    correct_answer = serializers.SerializerMethodField()

    class Meta:
        model = Answers
        fields = read_only_fields = ('answer_id', 'text', 'correct_answer',)

    def get_correct_answer(self, obj):
        question_type = obj.question_v.question_c.question_type
        if question_type == 2:
            return True
        if question_type == 0 or question_type == 1:
            return obj.answer_v.answer_c.correct_answer
        return None # future types


# test questions serializer
class TestQuestionSerializer(serializers.ModelSerializer):

    question_id = serializers.PrimaryKeyRelatedField(read_only=True)
    text = serializers.CharField(source='question_v.question_c.question_text')
    type = serializers.IntegerField(source='question_v.question_c.question_type')
    answers = serializers.SerializerMethodField()

    class Meta:
        model = Questions
        fields = read_only_fields = ('question_id', 'text', 'type', 'answers',)

    def get_answers(self, obj):
        answers_queryset = obj.question_v.answers_set
        answers_queryset = [answer for answer in answers_queryset.all() if not answer.deleted]
        return TestAnswerSerializer(answers_queryset, many=True).data


# test groups serializer
class TestGroupSerializer(serializers.ModelSerializer):

    group_id = serializers.PrimaryKeyRelatedField(read_only=True)
    description = serializers.CharField(source='group_v.group_c.group_description')
    questions = serializers.SerializerMethodField()
    position = serializers.SerializerMethodField()

    class Meta:
        model = Groups
        fields = read_only_fields = ('group_id', 'description', 'questions', 'position')

    def get_questions(self, obj):
        questions_queryset = obj.group_v.questions_set
        questions_queryset = [question for question in questions_queryset.all() if not question.deleted]
        return TestQuestionSerializer(questions_queryset, many=True).data

    def get_position(self, obj):
        return obj.test.positions.index(obj.group_id)+1


class TestContentSerializer(serializers.ModelSerializer):

    test_id = serializers.PrimaryKeyRelatedField(source='test_c.test_c_id', read_only=True)
    groups = serializers.SerializerMethodField()
    description = serializers.CharField(source='test_c.test_description')
    random = serializers.BooleanField(source='test_c.test_random')
    timer = serializers.IntegerField(source='test_c.test_timer')
    name = serializers.CharField(source='branch.branch_name')

    class Meta:
        model = Tests
        fields = ('test_id', 'description', 'random', 'timer', 'groups', 'name')

    def get_groups(self, obj):
        groups_queryset = obj.groups_set.filter(group_id__in=obj.positions)
        groups_queryset = groups_queryset.prefetch_related('group_v__group_c',
                                                           'group_v__questions_set__question_v__question_c',
                                                           'group_v__questions_set__question_v__answers_set__answer_v__answer_c',)
        return TestGroupSerializer(groups_queryset, many=True).data

    # test - serialized test content
    @staticmethod
    def content_as_hashtable(test):
        questions = {}
        answers = {}
        for group in test['groups']:
            for question in group['questions']:
                questions[question['question_id']] = {
                    'text': question['text'],
                }
                for answer in question['answers']:
                    answers[answer['answer_id']] = {
                        'text': answer['text'],
                    }
        return {
            'questions': questions,
            'answers': answers,
        }

