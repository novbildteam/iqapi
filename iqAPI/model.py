# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class AccountsProfile(models.Model):
    first_name = models.TextField(blank=True, null=True)
    second_name = models.TextField(blank=True, null=True)
    user = models.ForeignKey('AuthUser', models.DO_NOTHING, unique=True)
    email = models.TextField()
    secure_type = models.SmallIntegerField()
    smtp_port = models.IntegerField()
    smtp_server = models.TextField()

    class Meta:
        managed = False
        db_table = 'accounts_profile'


class Answers(models.Model):
    answer_id = models.AutoField(primary_key=True)
    question_v = models.ForeignKey('QuestionsV', models.DO_NOTHING, blank=True, null=True)
    answer_v = models.ForeignKey('AnswersV', models.DO_NOTHING, blank=True, null=True)
    deleted = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'answers'


class AnswersC(models.Model):
    answer_c_id = models.AutoField(primary_key=True)
    answer_text = models.TextField(blank=True, null=True)
    correct_answer = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 'answers_c'


class AnswersV(models.Model):
    answer_v_id = models.AutoField(primary_key=True)
    answer_c = models.ForeignKey(AnswersC, models.DO_NOTHING, blank=True, null=True)
    deleted = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'answers_v'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class Authtokens(models.Model):
    token_id = models.AutoField(primary_key=True)
    user_id = models.IntegerField()
    token = models.CharField(unique=True, max_length=100)
    type = models.SmallIntegerField()
    ip = models.BigIntegerField(blank=True, null=True)
    created = models.DateTimeField()
    disabled = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'authtokens'


class Branches(models.Model):
    branch_id = models.AutoField(primary_key=True)
    owner_id = models.IntegerField()
    branch_type = models.SmallIntegerField()
    current = models.IntegerField(blank=True, null=True)
    branch_name = models.CharField(max_length=150, blank=True, null=True)
    creation_time = models.DateTimeField(blank=True, null=True)
    editing_time = models.DateTimeField(blank=True, null=True)
    deleted = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'branches'


class Dirs(models.Model):
    dir_id = models.AutoField(primary_key=True)
    dir_name = models.CharField(max_length=100)
    owner_id = models.IntegerField()
    creation_time = models.DateTimeField(blank=True, null=True)
    deleted = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dirs'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class Environment(models.Model):
    var = models.CharField(unique=True, max_length=100)
    value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'environment'


class Fingerprint(models.Model):
    result_id = models.AutoField(primary_key=True)
    agent = models.CharField(max_length=500)
    device_id = models.CharField(max_length=50)
    time1 = models.IntegerField()
    time2 = models.IntegerField()
    date = models.DateTimeField()
    device_type = models.IntegerField(blank=True, null=True)
    hash = models.CharField(max_length=5, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'fingerprint'


class FsLinker(models.Model):
    item_id = models.AutoField(primary_key=True)
    parent = models.ForeignKey(Dirs, models.DO_NOTHING)
    item_type = models.SmallIntegerField()
    direction_id = models.IntegerField()
    owner_id = models.IntegerField(blank=True, null=True)
    link = models.IntegerField(blank=True, null=True)
    deleted = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'fs_linker'


class Groups(models.Model):
    group_id = models.AutoField(primary_key=True)
    test = models.ForeignKey('Tests', models.DO_NOTHING, blank=True, null=True)
    group_v = models.ForeignKey('GroupsV', models.DO_NOTHING, blank=True, null=True)
    deleted = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'groups'


class GroupsC(models.Model):
    group_c_id = models.AutoField(primary_key=True)
    group_description = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'groups_c'


class GroupsV(models.Model):
    group_v_id = models.AutoField(primary_key=True)
    group_c = models.ForeignKey(GroupsC, models.DO_NOTHING, blank=True, null=True)
    deleted = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'groups_v'


class Images(models.Model):
    image_id = models.AutoField(primary_key=True)
    user = models.ForeignKey('Users', models.DO_NOTHING)
    image_url = models.CharField(max_length=500, blank=True, null=True)
    deleted = models.NullBooleanField()
    preview_url = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'images'


class Lessons(models.Model):
    lesson_id = models.AutoField(primary_key=True)
    room = models.ForeignKey('Rooms', models.DO_NOTHING)
    subject = models.ForeignKey('Subjects', models.DO_NOTHING, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    start_time = models.DateTimeField(blank=True, null=True)
    end_time = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'lessons'


class Override(models.Model):
    override_id = models.AutoField(primary_key=True)
    room_id = models.IntegerField()
    test_id = models.IntegerField()
    position = models.SmallIntegerField(blank=True, null=True)
    timer = models.SmallIntegerField(blank=True, null=True)
    random = models.NullBooleanField()
    used = models.NullBooleanField()
    deleted = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 'override'


class PromoCodes(models.Model):
    code_id = models.AutoField(primary_key=True)
    code = models.CharField(unique=True, max_length=10)
    creation_time = models.DateTimeField(blank=True, null=True)
    used_time = models.DateTimeField(blank=True, null=True)
    comment = models.TextField(blank=True, null=True)
    subscriber = models.ForeignKey('SiteSubscribers', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'promo_codes'


class Questions(models.Model):
    question_id = models.AutoField(primary_key=True)
    group_v = models.ForeignKey(GroupsV, models.DO_NOTHING, blank=True, null=True)
    question_v = models.ForeignKey('QuestionsV', models.DO_NOTHING, blank=True, null=True)
    deleted = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'questions'


class QuestionsC(models.Model):
    question_c_id = models.AutoField(primary_key=True)
    question_text = models.TextField(blank=True, null=True)
    question_type = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'questions_c'


class QuestionsV(models.Model):
    question_v_id = models.AutoField(primary_key=True)
    question_c = models.ForeignKey(QuestionsC, models.DO_NOTHING, blank=True, null=True)
    deleted = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'questions_v'


class Results(models.Model):
    result_id = models.AutoField(primary_key=True)
    session = models.ForeignKey('Sessions', models.DO_NOTHING)
    question = models.ForeignKey(Questions, models.DO_NOTHING)
    answer = models.ForeignKey(Answers, models.DO_NOTHING, blank=True, null=True)
    test = models.ForeignKey('Tests', models.DO_NOTHING, blank=True, null=True)
    open_answer_text = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'results'


class ResultsTime(models.Model):
    time_id = models.AutoField(primary_key=True)
    session = models.ForeignKey('Sessions', models.DO_NOTHING)
    question = models.ForeignKey(Questions, models.DO_NOTHING)
    time = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'results_time'


class Rooms(models.Model):
    room_id = models.AutoField(primary_key=True)
    owner = models.ForeignKey('Users', models.DO_NOTHING)
    room_pass = models.CharField(max_length=100, blank=True, null=True)
    room_access = models.SmallIntegerField()
    room_status = models.BooleanField()
    queue = models.TextField(blank=True, null=True)  # This field type is a guess.
    show_results = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 'rooms'


class Sessions(models.Model):
    session_id = models.AutoField(primary_key=True)
    stream = models.ForeignKey('Streams', models.DO_NOTHING)
    working_user = models.ForeignKey('UsersRooms', models.DO_NOTHING)
    begin_time = models.DateTimeField(blank=True, null=True)
    end_time = models.DateTimeField(blank=True, null=True)
    del_reason = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'sessions'


class Shared(models.Model):
    share_id = models.AutoField(primary_key=True)
    who_user = models.ForeignKey('UserProfile', models.DO_NOTHING)
    with_user = models.ForeignKey('UserProfile', models.DO_NOTHING)
    item_type = models.SmallIntegerField()
    direction_id = models.IntegerField()
    read_only = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 'shared'


class ShortUrls(models.Model):
    url = models.CharField(unique=True, max_length=50, blank=True, null=True)
    room = models.ForeignKey(Rooms, models.DO_NOTHING)
    url_id = models.AutoField(primary_key=True)

    class Meta:
        managed = False
        db_table = 'short_urls'


class SiteSubscribers(models.Model):
    subscriber_id = models.AutoField(primary_key=True)
    email = models.CharField(max_length=100)
    first_name = models.CharField(max_length=100, blank=True, null=True)
    second_name = models.CharField(max_length=100, blank=True, null=True)
    ip = models.BigIntegerField(blank=True, null=True)
    time = models.DateTimeField(blank=True, null=True)
    job = models.CharField(max_length=1000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'site_subscribers'


class Streams(models.Model):
    stream_id = models.AutoField(primary_key=True)
    room = models.ForeignKey(Rooms, models.DO_NOTHING, blank=True, null=True)
    test = models.ForeignKey('Tests', models.DO_NOTHING)
    add_time = models.DateTimeField(blank=True, null=True)
    del_time = models.DateTimeField(blank=True, null=True)
    del_reason = models.TextField(blank=True, null=True)  # This field type is a guess.
    override = models.ForeignKey(Override, models.DO_NOTHING, blank=True, null=True)
    pace_mode = models.SmallIntegerField(blank=True, null=True)
    lesson = models.ForeignKey(Lessons, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'streams'


class Subjects(models.Model):
    subject_id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'subjects'


class Tests(models.Model):
    test_id = models.AutoField(primary_key=True)
    test_c = models.ForeignKey('TestsC', models.DO_NOTHING, blank=True, null=True)
    branch = models.ForeignKey(Branches, models.DO_NOTHING, blank=True, null=True)
    positions = models.TextField(blank=True, null=True)  # This field type is a guess.
    creation_time = models.DateTimeField(blank=True, null=True)
    deleted = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tests'


class TestsC(models.Model):
    test_c_id = models.AutoField(primary_key=True)
    test_description = models.TextField(blank=True, null=True)
    test_random = models.NullBooleanField()
    test_timer = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tests_c'


class UserProfile(models.Model):
    user = models.ForeignKey('Users', models.DO_NOTHING, primary_key=True)
    profile_first_name = models.CharField(max_length=100, blank=True, null=True)
    profile_second_name = models.CharField(max_length=100, blank=True, null=True)
    profile_description = models.CharField(max_length=200, blank=True, null=True)
    root = models.ForeignKey(Dirs, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'user_profile'


class Users(models.Model):
    user_id = models.AutoField(primary_key=True)
    user_login = models.CharField(unique=True, max_length=100)
    user_pass = models.CharField(max_length=100)
    user_salt = models.CharField(max_length=100)
    user_reg_time = models.DateTimeField(blank=True, null=True)
    user_login_time = models.DateTimeField(blank=True, null=True)
    type = models.SmallIntegerField(blank=True, null=True)
    last_activity_time = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'users'


class UsersAuth(models.Model):
    id = models.AutoField()
    user = models.ForeignKey(Users, models.DO_NOTHING)
    user_pass = models.CharField(max_length=100, blank=True, null=True)
    user_salt = models.CharField(max_length=100, blank=True, null=True)
    auth_via = models.TextField()  # This field type is a guess.
    social_id = models.CharField(max_length=25, blank=True, null=True)
    access_token = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'users_auth'


class UsersPromo(models.Model):
    user = models.ForeignKey(Users, models.DO_NOTHING)
    code = models.ForeignKey(PromoCodes, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'users_promo'


class UsersRooms(models.Model):
    working_user_id = models.AutoField(primary_key=True)
    user = models.ForeignKey(Users, models.DO_NOTHING, blank=True, null=True)
    first_name = models.CharField(max_length=100, blank=True, null=True)
    room_id = models.IntegerField()
    con_time = models.DateTimeField(blank=True, null=True)
    discon_time = models.DateTimeField(blank=True, null=True)
    second_name = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'users_rooms'
