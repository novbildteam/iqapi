import time
import hashlib
import requests
import images.settings as settings

class ImageUploadError(Exception):
    pass

class ImageServiceSetupError(Exception):
    pass


class ImageServiceSetting:
    def __init__(self, required=True):
        self.required = required

class ImageService:

    #required_settings = []

    setting_field_name = 'setting'

    def __init__(self):
        classname = self.__class__.__name__
        # try get dict with settings
        try:
            dict_settings = settings.settings[classname]
        except KeyError:
            raise ImageServiceSetupError('no settings')
        # is it dict?
        if type(dict_settings) != dict:
            raise ImageServiceSetupError('settings is not dict')

        # all fields with 'setting_field_name' prefix is special settings fields
        class_fields = self.__class__.__dict__
        for key, value in class_fields.items():
            if key.find(self.setting_field_name+'_') == 0:
                dict_key = key[len(self.setting_field_name)+1:]
                if dict_key not in dict_settings:
                    raise ImageServiceSetupError('"{}" is not defined in settings'.format(dict_key))
                if type(class_fields[key]) == type(dict_settings[dict_key])\
                    or class_fields[key] is None:
                    self.__dict__[key] = dict_settings[dict_key]
                else:
                    raise ImageServiceSetupError('"{}" type conflict'.format(dict_key))



class CloudinaryImageService(ImageService):

    setting_api_url = setting_api_key = setting_private_key = setting_upload_preset = None

    #required_settings = ['api_url', 'res_url', 'api_key', 'private_key', 'upload_preset']

    # cloudiary api & res urls

    # api_url = 'https://api.cloudinary.com/v1_1/instaquiz'
    # res_url = 'https://res.cloudinary.com/instaquiz/image/upload'
    #
    # # cloudinary keys
    # api_key = '571447163427754'
    # private_key = 'Hy8mZJKgN3bsNCACllgB7hpxjc0'
    #
    # upload_preset = 'xwkspltg'

    def upload(self, data, upload_preset=None):
        if not upload_preset:
            upload_preset = self.setting_upload_preset
        timestamp = str(int(time.time()))  # current time
        # making signature (see cloudinary API)

        signature = ('timestamp={}&upload_preset={}{}')\
            .format(timestamp, upload_preset, self.setting_private_key)\
            .encode('utf-8')
        signature = hashlib.sha1(signature).hexdigest()

        r = requests.post(self.setting_api_url + '/image/upload',
                          data={'timestamp': timestamp, 'api_key': self.setting_api_key, 'signature': signature,
                                'upload_preset': upload_preset}, files={'file': data})
        if r.status_code == requests.codes.ok:
            url = r.json()['url']
            prev_url = self._make_preview_url(url)  # get URL preview image
            response = {
                'image_url': url,
                'preview_url': prev_url,
            }
            return response
        else:
            raise ImageUploadError(r.status_code)

    def _make_preview_url(self, url):
        pttrn = 'private/'
        transform_name = 't_media_lib_thumb'
        index = url.find(pttrn)
        if index == -1:
            return None
        index += len(pttrn)
        index2 = url.find('/', index)
        new_url = url[0:index2 + 1] + transform_name + url[index2:]
        return new_url


def upload_image(data):
    service = CloudinaryImageService()
    return service.upload(data)


if __name__ == '__main__':
    service = CloudinaryImageService()