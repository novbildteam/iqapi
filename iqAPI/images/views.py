from django.shortcuts import render
from django.db.models import Q
from django.db import Error
from django.views.decorators.csrf import csrf_exempt
import django_filters

from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import detail_route, list_route, api_view, permission_classes, parser_classes
from rest_framework import viewsets, views
from rest_framework.parsers import FileUploadParser, MultiPartParser, FormParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.filters import OrderingFilter

from iqAPI import error_codes

from images.models import Images
from images.filters import IsOwnerImagesFilter
from images.serializers import ImagesSerializer
import images.tools as tools

# Create your views here.

# class FileUploadView(views.APIView):
#     parser_classes = (MultiPartParser, FormParser, )
#
#     def post(self, request):
#         print(request.data)
#         print(request.files)
#         return Response('')

@api_view(['POST'])
@permission_classes([IsAuthenticated, ])
@parser_classes([MultiPartParser, FormParser])
def upload(request):
    try:
        image_info = tools.upload_image(request.data['file'])
    except tools.ImageUploadError as e:
        return Response({
            'error_code': error_codes.IMAGE_LOADING_ERROR
        })
    try:
        img = Images.objects.create(user=request.user,
                                image_url=image_info['image_url'],
                                preview_url=image_info['preview_url'])
        img.save()
    except Error:
        return Response({},status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    return Response({
        'image_id': img.image_id,
        **image_info})

class ImagesViewSet(viewsets.ModelViewSet):

    queryset = Images.objects.filter(Q(deleted=None) | Q(deleted=False))

    http_method_names = ('get', 'delete',)

    permission_classes = (IsAuthenticated,)

    filter_backends = (django_filters.rest_framework.DjangoFilterBackend,
                       OrderingFilter,
                       IsOwnerImagesFilter,)
    serializer_class = ImagesSerializer

    ordering_fields = ('image_id', )

    # override default DELETE method
    def destroy(self, request, *args, **kwargs):
        image = self.get_object()
        image.deleted = True
        try:
            image.save(update_fields=['deleted'])
            return Response({
                'deleted': True,
            }, status=status.HTTP_200_OK)
        except Error:
            return Response('', status=status.HTTP_500_INTERNAL_SERVER_ERROR)


    # is it really neseccary?
    # def get_paginated_response(self, data):
    #     response = super().get_paginated_response(data)
    #     response.data['images'] = response.data['results']
    #     response.data.pop('results')
    #     return response