from django.db import models
from iqAuth.models import Users

class Images(models.Model):
    image_id = models.AutoField(primary_key=True)
    user = models.ForeignKey(Users, models.DO_NOTHING)
    image_url = models.CharField(max_length=500, blank=True, null=True)
    deleted = models.NullBooleanField()
    preview_url = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'images'

