from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from images import views

router = DefaultRouter()
router.register(r'', views.ImagesViewSet)

urlpatterns = [
    url(r'^upload/$', views.upload),
    url(r'^', include(router.urls)),
]