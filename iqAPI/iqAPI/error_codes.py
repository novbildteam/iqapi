
# COMMON ERRORS

# 1	Неверная группа запроса
# 2	Неверный метод запроса
# 3	Неверный тип запроса
# 4	Не все требуемые параметры
# 5	Неверное значение параметра
# 6	Время жизни токена истекло
# 7	Доступ запрещен
# 8	Неверный токен
# 9	Неверный id потока
# 10	Неверный id директории
# 1010	Метод не реализован

WRONG_GROUP = 1
WRONG_METHOD = 2
WRONG_REQUEST_TYPE = 3
NOT_ENOUGH_PARAMS = 4
WRONG_PARAM_VALUE = 5
LIFETIME_TOKEN = 6
FORBIDDEN = 7
WRONG_TOKEN = 8
WRONG_URLID = 9 # urlid?
WRONG_DIR_ID = 10
NOT_IMPLEMENTED = 1010

#images

IMAGE_LOADING_ERROR = 601

