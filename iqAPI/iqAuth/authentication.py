import logging

from rest_framework import authentication
from rest_framework import exceptions

from iqAuth.models import UsersAuth, Users, Authtokens
import iqAPI.error_codes as error_codes


logger = logging.getLogger(__package__)


class IQAuthentication(authentication.BaseAuthentication):
    def authenticate(self, request):
        token = request.query_params.get('token')
        if not token:
            logger.info('no token')
            raise exceptions.NotAuthenticated({
                'error_code': error_codes.FORBIDDEN,
                'message': 'No token',
            })

        try:
            authtoken = Authtokens.objects.get(token=token)
            user = Users.objects.get(user_id=authtoken.user_id)
        except (Users.DoesNotExist, Authtokens.DoesNotExist):
            logger.info('invalid token %s', token)
            raise exceptions.AuthenticationFailed({
                'error_code': error_codes.WRONG_TOKEN,
                'message': 'invalid token'
            })
        return (user, None)
