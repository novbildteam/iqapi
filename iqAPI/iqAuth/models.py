from django.db import models
from django.contrib.auth.models import AbstractBaseUser

class UserProfile(models.Model):
    user = models.ForeignKey('Users', models.DO_NOTHING, primary_key=True)
    profile_first_name = models.CharField(max_length=100, blank=True, null=True)
    profile_second_name = models.CharField(max_length=100, blank=True, null=True)
    profile_description = models.CharField(max_length=200, blank=True, null=True)
#    root = models.ForeignKey(Dirs, models.DO_NOTHING)


    class Meta:
        managed = False
        db_table = 'user_profile'


class Users(AbstractBaseUser):
    user_id = models.AutoField(primary_key=True)
    user_login = models.CharField(unique=True, max_length=100)
    password = models.CharField(max_length=100, db_column='user_pass')
    user_salt = models.CharField(max_length=100)
    user_reg_time = models.DateTimeField(blank=True, null=True)
    last_login = models.DateTimeField(blank=True, null=True, db_column='user_login_time')
    type = models.SmallIntegerField(blank=True, null=True)
    last_activity_time = models.DateTimeField(blank=True, null=True)

    is_active = True

    USERNAME_FIELD = 'user_login'
    REQUIRED_FIELDS = []

    def get_full_name(self):
        return self.first_name + ' ' + self.second_name

    def get_short_name(self):
        #return self.profile_first_name
        return self.first_name

    @property
    def first_name(self):
        return UserProfile.objects.get(user__user_id=self.user_id).profile_first_name

    @property
    def second_name(self):
        return UserProfile.objects.get(user__user_id=self.user_id).profile_second_name

    @property
    def is_staff(self):
        return False

    class Meta:
        managed = False
        db_table = 'users'


class UsersAuth(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(Users, models.DO_NOTHING)
    user_pass = models.CharField(max_length=100, blank=True, null=True)
    user_salt = models.CharField(max_length=100, blank=True, null=True)
    auth_via = models.TextField()  # This field type is a guess.
    social_id = models.CharField(max_length=25, blank=True, null=True)
    access_token = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'users_auth'



class Authtokens(models.Model):
    token_id = models.AutoField(primary_key=True)
    user_id = models.IntegerField()
    token = models.CharField(unique=True, max_length=100)
    type = models.SmallIntegerField()
    ip = models.BigIntegerField(blank=True, null=True)
    created = models.DateTimeField()
    disabled = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'authtokens'